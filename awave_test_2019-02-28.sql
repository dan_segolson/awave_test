# Dump of table adress_books
# ------------------------------------------------------------

DROP TABLE IF EXISTS `adress_books`;

CREATE TABLE `adress_books` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `birth_date` varchar(255) DEFAULT NULL,
  `owner_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`)
  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `adress_books` WRITE;
/*!40000 ALTER TABLE `adress_books` DISABLE KEYS */;

INSERT INTO `adress_books` (`id`, `full_name`, `email`, `image_url`, `birth_date`, `owner_id`, `created`)
VALUES
  (1,'Dan Segolson','dmsproject@gmail.com','http://localhost/awave_test/uploads/th/th_1.png','1965-02-17',1,'2019-02-26 14:56:57'),
  (2,'Daniela Karlsson','d.msproject@gmail.com','http://localhost/awave_test/uploads/th/th_2.jpg','1948-12-22',2,'2019-02-26 16:02:38'),
  (3,'Mad surfer','dm.sproject@gmail.com','http://localhost/awave_test/uploads/th/th_3.png','1965-02-17',1,'2019-02-27 13:21:37'),
  (7,'Kenzo the Dog','dms.project@gmail.com','http://localhost/awave_test/uploads/th/th_7.jpg','2001-11-01',1,'2019-02-27 13:40:51'),
  (8,'Robin rubin','dmsp.roject@gmail.com','http://localhost/awave_test/uploads/th/th_8.jpg','2002-11-16',1,'2019-02-27 13:45:19'),
  (9,'Martin Karlsson','dmspr.oject@gmail.com','http://localhost/awave_test/uploads/th/th_9.jpg','1965-02-27',2,'2019-02-27 14:35:30'),
  (10,'Kalle Boll','dmspro.ject@gmail.com','http://localhost/awave_test/uploads/th/th_10.jpg','1974-02-27',1,'2019-02-27 18:58:26');

/*!40000 ALTER TABLE `adress_books` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(128) NOT NULL DEFAULT '',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `role`, `created`)
VALUES
  (1,'admin','2019-02-26 10:01:23'),
  (2,'user','2019-02-26 10:01:37');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `is_logged_in` int(11) DEFAULT NULL,
  `authentication_token` varchar(255) DEFAULT NULL,
  `role` int(11) unsigned NOT NULL DEFAULT '2',
  `last_login` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role` (`role`)
  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `full_name`, `password`, `is_logged_in`, `authentication_token`, `role`, `last_login`, `created`)
VALUES
  (1,'DmS','Dan Segolson','$2y$10$hVOXGJfGNNWmOjCWMratPOVuw9gcIcewViEcoF95HKcr84MYtBDhK',0,'a1e1ff7c27dc8ba43a7c0f03a3c92b4c',2,'2019-02-28 10:17:01','2019-02-26 12:01:06'),
  (2,'admin','Worktest admin','$2y$10$c5/z.7zEsMFe.klQADwD7u3W1XSCdou31T.Z6FFwt0.fbcLf2uKNW',0,'d7017d128b2e4390cc1f46f468e9fd4b',1,'2019-02-28 10:11:24','2019-02-26 12:38:26');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


/* Add Foreign keys */
ALTER TABLE adress_books 
ADD CONSTRAINT owner_fk FOREIGN KEY (owner_id) REFERENCES users(id) 
ON DELETE NO ACTION 
ON UPDATE NO ACTION;

ALTER TABLE users  
ADD CONSTRAINT roles_fk FOREIGN KEY (role) REFERENCES roles(id) 
ON DELETE NO ACTION 
ON UPDATE NO ACTION;