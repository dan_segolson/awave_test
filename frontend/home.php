<?php 
require_once('../conf/main_conf.php');
require_once(DIR_APPLICATION.'conf/conf.router.php');
$contr_inc = DIR_APPLICATION.'controller/'.$controller;

if (!file_exists($contr_inc)) {
    $contr_inc = DIR_APPLICATION.'controller/main/ctrl.index.php';
}
require_once($contr_inc);
?>

<?php 
require_once(DIR_TEMPLATES.'main/tpl.header.php');
?>

<body class="intro-2 fixed-sn light-blue-skin">

    <header>
    
    <!--Top Navigation-->
        <?php 
        require_once(DIR_TEMPLATES.'main/tpl.inner-top-nav-drop.php');
        ?> 
    <!--Top Navigation-->
    
    </header>

    <div class="container-fluid">
        <div class="row">
            <!--Side Navigation-->
            <?php 
            require_once(DIR_TEMPLATES.'main/tpl.side-nav.php');
            ?> 
        <!--Side Navigation-->
            <main  class="col-sm-9 ml-sm-auto col-md-10 pt-3">  
            <!-- Start your content here-->
                <?php 
                require_once(DIR_TEMPLATES.$tpl_content);
                ?>
            <!-- /Start your content here-->
            </main>
        </div>
    </div>

<!-- FOOTER template -->
<?php 
require_once(DIR_TEMPLATES.'main/tpl.footer.php');
?>
 
