<?php 
require_once('../conf/main_conf.php');

require_once(DIR_APPLICATION.'conf/conf.router.php');
$contr_inc = DIR_APPLICATION.'controller/'.$controller;
if (!file_exists($contr_inc)) {
    //kill any logged in session to ensure that you are tossed out if you try anything funny...
    $contr_inc = DIR_APPLICATION.'controller/ctrl.index.php';
}
require_once($contr_inc);
?>

<?php 
require_once(DIR_TEMPLATES.'main/tpl.header.php');
?>

<body>

    <header>

        <!--Main Navigation-->
        <?php 
        require_once(DIR_TEMPLATES.'main/tpl.top-nav.php');
        ?>
        <!--Main Navigation-->
        
        <!--Login Section-->
        <?php 
        require_once(DIR_TEMPLATES.'main/tpl.login.php');
        ?>

    </header>

    <main>
        <!-- Start your content here-->
        
        <!-- /Start your content here-->
    </main>
<!-- FOOTER template -->
<?php 
require_once(DIR_TEMPLATES.'main/tpl.footer.php');
?>
 
