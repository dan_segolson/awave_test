<?php 
require_once('../conf/main_conf.php');

$pagerole = 'login';
$pagename = 'Logging in...';
$navstyle = 'navbar-dark';
$html_class = 'full-height';
?>


<?php 
require_once(DIR_TEMPLATES.'main/tpl.header.php');
?>

<body>

<header>

<!--Main Navigation-->
    <?php 
    require_once(DIR_TEMPLATES.'main/tpl.top-nav.php');
    ?>


<!--Login Section-->
    <?php 
    require_once(DIR_TEMPLATES.'main/tpl.loader.php');
    ?>

</header>

<main>
<!-- page content template -->
    
</main>

<!-- FOOTER template -->
<?php 
require_once(DIR_TEMPLATES.'main/tpl.footer.php');
?>
 
