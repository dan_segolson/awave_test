
<div class="container ">
  <div class="row">
    <div class="col">
      <!--Panel for basic find & list-->  
      <div class="card">
        <div class="card-header bg-info text-white">
             Sök i adressboken
        </div>
        <div class="card-body">
          <p class="card-text">
            Sök efter kontakter i adressboken (om tomt hämtas allt)
        </p>
        <br>
        <hr>          
            <form action="page/list/action/find" method="post">
            <div class="row ">

              <div class="col-md-6">
	            <div class="md-form">
	              <div class="form-group">
	                  <input type="text" id="search" name="search" class="form-control" value="<?php echo $_POST['search']?>">
	                  <label for="search">Sök på del av namn (minst tre tecken)</label>
	              </div>

	            <div class="form-group">
	                <button role="submit" id="btn_users" name="btn_users"  class="btn btn-success">Sök</button>
	            </div>
	            </div>
	          </div>
	          
	          </div>
	            <hr>
	          <p>Kontakter</p>
	          <?php 
	          if($response->error){
	          	print('<div class="alert alert-danger">');
	                print('<p>'.$response->error.'</p>');
	            print('</div>');
	          }
	           ?>
	          <div id="delete-info-msg"></div>
	          <table id="adress_book_table" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Profilbild</th>
                        <th>Namn</th>
                        <th>Email</th>
                        <th>Födelsedag</th>
                        <th>Inlagd</th>
                        <th>Från Adressbok</th>
                        <th>Action</th>
                    </tr>
                </thead>
               
                <tbody>
                  <?php
                  if(is_array($response->data)){
                    foreach ($response->data as $key => $item) {
                      print('<tr id="'.$item->id.'">');
                        print('<td>'.$item->id.'</td>');
                        print('<td><img class="profile_image_small" src="'.$item->image_url.'"></td>');
                        print('<td>'.$item->full_name.'</td>');
                        print('<td>'.$item->email.'</td>');
                        print('<td>'.$item->birth_date.'</td>');
                        
                        print('<td>'.$item->created.'</td>');
                        print('<td>'.$item->username.'</td>');
                        print('<td><a class="btn btn-warning"  href="page/edit/action/load/id/'.$item->id.'">Edit</a> <button type="button" class="btn btn-danger"  onclick="return deleteRow('.$item->id.')">Delete</a></td>');
                      print('</tr>');
                    }
                  }
                  
                  ?>              
                   
                   

                </tbody>
            </table>
        </form>
      </div>
    </div>
      <!--/.Panel-->
    </div>
  </div>
</div>




    


    