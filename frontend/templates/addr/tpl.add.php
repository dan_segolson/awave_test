
<div class="container ">
	<div class="row">
		<div class="col">
		  <!--Panel for base team selection-->	
		  <div class="card">
		    <div class="card-header bg-info text-white">
		          Skapa kontakt
		    </div>
		    <div class="card-body">
				<p class="card-text">
		            <?php
		            if($role == 1){
		            	print('Först väljer du en användare för att skapa kontakten åt.<br> Skapa sen en kontakt i användarens adressbok.<br>');
		            	
		            }else{
		            	print('Skapa en kontakt i din adressbok.<br>');
		            }
		            ?>
		        </p>

		        <hr>          

		    <?php 
		    	if($response->data[0]){
		    		//check if you  are trying to edit a contct you own, or if you are admin
		    		if(($response->data[0]->owner_id == $user_id) || ($role == 1)){
		    			$contact = $response->data[0];	
		    		}else{
		    			print('<div class="alert alert-danger">');
		                print('<p>You do not have the correct permissions to edit this contact</p>');
		            	print('</div>');
		    		}
		    		
		    	}

		    	if($response->error != ''){
		    		print('<div class="alert alert-danger">');
	                print('<p>'.$response->error.'</p>');
	            	print('</div>');

		    	}
		    	if($response->status != ''){
		    		print('<div class="alert alert-success">');
	                print('<p>'.$response->status.'</p>');
	            	print('</div>');
	            }
		    ?>
            <form action="page/add/action/add/" method="post" enctype="multipart/form-data">
	            <div class="row ">
	              <div class="col-md-6">
		            <div class="md-form">
		              <div class="form-group">
		                  <input type="hidden" id="id" name="id" class="form-control" value="<?php echo $contact->id ?>">
		                   
		                   <input type="hidden" id="image_url" name="image_url" class="form-control" value="<?php echo $contact->image_url ?>">
		              </div>

		            <?php
		            	if($role == 1){
		            		print('<div class="form-group">');
		            		print('<select id="owner_id" name="owner_id" class="custom-select custom-select-md mb-3">');
								print('<option selected>Välj vems adressbok</option>');
								foreach ($users->data as $key => $user) {
									print('<option value="'.$user->id.'">'.$user->full_name.' : '.$user->user_role.'</option>');
								}
							print('</select>');
		            		print('</div>');
		            	}else{
		            		print('<input type="hidden" id="owner_id" name="owner_id" class="form-control" value="'.$user_id.'">');
		            	}

		            ?>

		              <div class="form-group">
		              	<div class=" custom-file">
		                  <input type="file" id="profile_image" name="profile_image" class="custom-file-input" value="<?php echo $contact->image_url ?>" accept="image/x-png,image/gif,image/jpeg">
		                  <label class="custom-file-label" for="profile_image">Välj profilbild</label>
		              	</div>
		              	<br>
		              	<?php 
		              	if($contact->image_url != ''){
		              		print('<img class="profile_image_small" src="'.$contact->image_url.'"> Nuvarande bild<br>');
		              	}
		              	?>
		              	<br>
		              </div>

		              <div class="form-group">
		                  <input type="text" id="full_name" name="full_name" class="form-control" value="<?php echo $contact->full_name ?>">
		                  <label for="full_name">Förnamn och efternamn</label>
		              </div>

		              <div class="form-group">
		                  <input type="text" id="email" name="email" class="form-control" value="<?php echo $contact->email ?>">
		                  <label for="email">Email</label>
		              </div>

		              <div class="form-group">
		                  <input type="text" id="birth_date" name="birth_date" class="form-control" value="<?php echo $contact->birth_date ?>">
		                  <label for="search">Födelsedag (YYYY-MM-DD)</label>
		              </div>

		            <div class="form-group">
		                <button role="submit" id="btn_update" name="btn_update"  class="btn btn-warning">Uppdatera</button>
		            </div>
		            </div>
		          </div>
		          
		        </div>
		    </form>

			</div>
		</div>
      <!--/.Panel-->
		</div>
	</div>
</div>
