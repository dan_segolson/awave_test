
<div class="container ">
  <div class="row">
    <div class="col">
      <!--Panel for base team selection-->  
      <div class="card">
        <div class="card-header bg-info white-text">
              Start
        </div>
        <div class="card-body">
          <p class="card-text">
            <?php 
            print('Välkommen till adressboken '.$username);
            ?>
          </p>
          <p>
            <?php
            if($role == 1){
              print('Då du är inloggad som admin så har du möjlighet att administrera samtliga användares poster i adressboken');
            }else{
              print('Som en normal användare så kan du skapa och administrera dina poster i adressboken.');
            }
            ?> 
        </p>
          
        <br>
        <hr>          

      </div>
    </div>
      <!--/.Panel-->
    </div>
  </div>
</div>




    