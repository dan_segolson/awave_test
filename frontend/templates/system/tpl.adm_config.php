<div class="container ">
	<div class="row">
		<div class="col">
		  <!--Panel for base team selection-->	
			<div class="card">
				<div class="card-header primary-color lighten-1 white-text">
					Admin panel settings
				</div>
				<div class="card-body">
					<p class="card-text red-text">
						Environment selection for the admin system.<br>
						Switching sets a cookie to decide if you are using live or stage. <br>
						If the cookie is missing, the system defaults to LIVE !
					</p>
					<br>
					<hr>
					<form action="<?php echo $_SERVER['PHP_SELF'] ?>?page=adm_config&action=setEnv" method="post"  >

				        <div class="row ">

				        	<div class="col-md-6">

				        		<div class="md-form">
							        <select id="sel_environment" name="sel_environment" class="mdb-select colorful-select dropdown-primary">
								    	<option value="live">Use LIVE db and api</option>
								    	<option value="stage">Use STAGE db and api</option>				
								    </select>
								    <label for="sel_environment">Select environment to admin (always defaults to LIVE)</label>
								    <?php
								    $sEnv = 'LIVE';
								    if($_SESSION['USE_ENV'] != ''){
								    	$sEnv = $_SESSION['USE_ENV'];
								    }
								    print('<div id="error-msg" class="danger-color-dark white-text"> &nbsp;&nbsp;Currently using '.$sEnv.' environment</div>');
								    ?>
							    </div>

							    <div class="md-form">
							    	<button role="submit" class="btn btn-danger">Change environment to administrate</button>
							    </div>
							</div>


						</div>
					</form>
				</div>
			</div>
      <!--/.Panel-->
		</div>
	</div>
</div>