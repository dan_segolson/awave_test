
    <?php  
    //set filetime as version to force no-cache on changed file
    $path = DIR_FRONTEND;
    $v1 = filemtime($path.'appjs/footer_init_scripts.js');
    $v2 = filemtime($path.'appjs/login.js');
    ?>
    <!-- SCRIPTS -->
    <!-- JQuery & Bootstrap-->
   
  
   <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- External login scripts, hookup and basic integration-->
    <script type="text/javascript" src="appjs/login.js?v=<?php echo $v2 ?>">"></script>  


    <!-- Datables scripts  -->
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.2/b-html5-1.5.2/b-print-1.5.2/datatables.min.js"></script>


    <script type="text/javascript" src="appjs/footer_init_scripts.js?v=<?php echo $v1 ?>"></script>
 

    <!-- AREA SPECIFIC JS FOR BACKEND CALLS  -->
    <?php echo $js_backend_include ?>
</body>

</html> 
