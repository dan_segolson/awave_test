    <section class="login-page ">
        <div class="container ">
            <div class="d-flex justify-content-center">
                <div class="row flex-center pt-5 mt-3">
                    <div class="col-md-6 text-center text-md-left mb-5">
                        <div class="white-text">
                            <h1 class="display-4 " data-wow-delay="0.3s">Logging you in...</h1>
                            <hr class="hr-light">
                            <h6 class="" >In a moment you will be transferred to the admin panel - should you have any problems please return to login</h6>
                            <br>
                            <a href="index.php" class="btn btn-outline-neutral">Return to login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
    //simulate login...
        setTimeout(redirect, 2000);
        function redirect(){
            window.location = 'home.php';
        }
    </script>