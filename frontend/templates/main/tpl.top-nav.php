    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="<?php ($pagerole == 'login' ? print('index.php') : print('home.php')); ?>"><img src="img/brand/awave_logo.png" class="img-fluid brand-logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                <ul class="navbar-nav mr-auto">
            <?php if($routingpage == 'home') { 
                foreach ($nav_links['top'] as $type => $item) {
                    print('<li class="nav-item btn-group">
                            <a class="nav-link waves-effect" id="'.$type.'" href="'.$item['link'].'">'.$item['name'].'</a>
                        </li>');   
                }
            }
            ?>
                </ul>
                <?php if($routingpage == 'home') { ?>
                <form class="form-inline">
                    <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                </form>
                <?php } ?>
            </div>
        </div>
    </nav>
