
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
        
        <ul class="nav navbar-nav nav-flex-icons mr-auto">
        <?php if($routingpage == 'home') { 
            foreach ($nav_links['top'] as $type => $item) {
                print('<li class="nav-item">
                        <a href="'.$item['link'].'" id="'.$type.'" class="nav-link waves-effect">'.$item['name'].'</a>
                    </li>');        
            }
        }
        ?>
        <li class="nav-item"><a href="home.php?page=home" class="button-collapse"><i class="fa fa-home"></i></a></li>
        </ul>

        <!-- Breadcrumb-->
        <div class="breadcrumb-dn ml-auto">
            <a class="" href="#"><strong><?php echo $pagename ?></strong></a>
        </div>
    </div>
    </nav>
    <script>
        function logOut(){
            document.getElementById('logout').submit();
        }
    </script>
