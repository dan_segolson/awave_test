    <!--Navbar-->
    <?php
    $sEnv = $_SESSION['USE_ENV'];
    if($sEnv == ''){
        $sEnv = 'LIVE';
    }
    ?>


    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="<?php ($pagerole == 'login' ? print('index.php') : print('home.php')); ?>"><img src="img/brand/awave_logo.png" class="img-fluid brand-logo"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">


            <?php 
            if($routingpage == 'home') { 
                foreach ($nav_links['top'] as $type => $item) {
                    if($type != 'logout'){
                    print('<span class="nav-item dropdown">
                            <a href="#" id="'.$type.'" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'.$item['name'].'</a>
                        <div class="dropdown-menu">');
                    
                        foreach ($nav_links[$type] as $key => $menu) {
                            if($menu['link'] == '#'){
                                print('<span class="dropdown-itemX">&nbsp;&nbsp;<strong>'.$menu['name'].'</strong></span>');
                                print('<div class="dropdown-divider"></div>');
                            }else{
                                print('<a class="dropdown-item" href="'.$menu['link'].'">'.$menu['name'].'</a>');    
                            }
                            
                        }
                        print('</div></span>');
                    }else{
                        print('<span class="nav-item">
                            <a href="'.$item['link'].'" id="'.$type.'" class="nav-link waves-effect">'.$item['name'].'</a>
                        </span>'); 
                    }              
                }
            }
            ?>

            </ul>
            
            <!-- Breadcrumb-->
            <div class="breadcrumb-dn ml-auto">
                <p class="navbar-text">Inloggad som: <?php echo $username ?> <br> 
                <strong><?php echo $pagename ?></strong></p>
            </div>
        </div>
    </nav>
    <script>
        function logOut(){
            document.getElementById('logout').submit();
        }
    </script>
