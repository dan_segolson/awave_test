<!DOCTYPE html>
<?php 
$url =  $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; 
$parsed = parse_url($url);
$parts = explode('/',$parsed['path']);
$base_url = '//';
foreach ($parts as $key => $value) {
    $base_url .= $value.'/';
    if($value == 'frontend'){
        break;
    }
}
?>
 
<html lang="en" class="<?php echo $html_class ?>">
<?php
$path = DIR_FRONTEND;
$v = filemtime($path.'css/style.css');
?>
<head>
    <?php print('<base href="'.$base_url.'">'); ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo SITE_NAME ?> v1.0 - <?php echo $pagename ?></title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet">  -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">

    <!-- Data tables css -->
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.2/b-html5-1.5.2/b-print-1.5.2/datatables.min.css"/>

    <!-- Your custom styles (optional) -->
    <link href="css/style.css?v=<?php echo $v ?>" rel="stylesheet">
</head>
