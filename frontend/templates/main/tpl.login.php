    
<div class="container ">
  <div class="row clear-top">
    <div class="col">
      <!--Panel for base team selection-->  
      <div class="card col-md-6 col-xl-5 offset-xl-3">
        <div id="form_holder" class="">
            <!--Form-->
            <form id="login_form" action="javascript:handleLogin()" method="post">
                <div class="">
                    <div class="card-body z-depth-2">
                        <!--Header-->
                        <div class="text-center">
                            <h3>Login</h3>
                            <div id="error_msg" class="alert alert-danger" role="alert"></div> 
                            <hr>
                        </div>
                        <!--Body-->
                            <div class="md-form">
                                <i class="fa fa-user prefix grey-text"></i> <label for="username">Email</label>
                                <input type="text" name="username" id="username" class="form-control">
                                
                            </div>
                            <br>
                            <div class="md-form"> 
                                <i class="fa fa- fa-lock prefix grey-text"></i> <label for="pass">Password</label>
                                <input type="password" name="password" id="pass" class="form-control">
                                
                            </div>
                            <br>
                            <div class="text-center">
                                <button type="submit" id="loginSubmit" class="btn btn-success">Login</button> or <a href="javascript:showRegister()">register</a>                                     
                            </div>
                    </div>
                </div>
            </form>
            <!--/.Form-->

            <form id="register_form" action="javascript:handleRegister()" method="post">
                <!-- <div class="cardX"> -->
                    <div class="card-body z-depth-2">
                        <!--Header-->
                        <div class="text-center">
                            <h3>Register</h3>
                            <div id="reg_error_msg" class="alert alert-danger" role="alert"></div> 
                            <hr>
                        </div>
                        <!--Body-->
                            <div class="md-form">
                                <label for="new_username">Username</label>
                                <input type="text" name="new_username" id="new_username" class="form-control" required>
                                <small id="new_username_help" class="form-text text-muted">Your selected username, will be used at login, min 3 chars</small>
                            </div>
                            <br>
                            <div class="md-form">
                                <label for="full_name">Full name</label>
                                <input type="text" name="full_name" id="full_name" class="form-control" required>
                                <small id="full_name_help" class="form-text text-muted">Your full name</small>
                            </div>
                            <br>
                            <!-- <div class="md-form">
                                <label for="company">Your company</label>
                                <input type="text" name="company" id="company" class="form-control" required>
                                <small id="company_help" class="form-text text-muted">Name of your company or equivivalent</small>
                            </div>
                            <br>   
                            <div class="md-form">
                                <label for="company_url">Full URL </label>
                                <input type="text" name="company_url" id="company_url" class="form-control" required onfocus="clearMsg()" onblur="validateURL(this.value)">
                                <small id="company_url_help" class="form-text text-muted">Full url (ex http://canoes.ppisw.com/ ) to the wordpress site which will use the payforaccess plugin</small>
                            </div> -->
                            <div id="url_msg" class="" role="alert"></div>
                            <br>   
                            <label for="clear_pw">Show passwords as text</label>
                            <input type="checkbox" name="clear_pw" onclick="viewPass()"> 
                            <div class="md-form">
                                <label for="password_1">Password</label>
                                <input type="password" name="password_1" id="password_1" class="form-control">
                                <small id="password_1_help" class="form-text text-muted">Enter your password, min 8 mixed chars</small>
                            </div>
                            <br>
                            <div class="md-form">
                                <label for="password_2">Repeat Password</label>
                                <input type="password" name="password_2" id="password_2" class="form-control">
                                <small id="password_2_help" class="form-text text-muted">Repeat your password</small>
                            </div>
                            <div id="pass_compare" class="" role="alert"></div>
                            <br>
                            <div class="text-center">
                                <button type="submit" id="registerSubmit" class="btn btn-success" onclick="comparePass()">Register</button> or <a href="javascript:showLogin()">login</a>                                      
                            </div>
                    </div>
                <!-- </div> -->
            </form>
        </div>
    </div>
      <!--/.Panel-->
    </div>
  </div>
</div>
