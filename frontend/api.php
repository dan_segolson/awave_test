<?php
require_once('../conf/main_conf.php');
require_once(DIR_APPLICATION.'model/api/class.InterfaceApi.php');

/**
 * Extends InterfaceApi in order to override the validation switch to true and starting the RestServer.
 * When validation true the api will validate in each request to ensure that each request is done from an approved client api-key
*/
class ValidatedInterfaceApi extends InterfaceApi {

	public function __construct(){
		$api = new InterfaceApi();
		$api->setValidate();
	    $rest = new RestServer($api);
	    $rest->handle();
	}
}

$web_interface = new ValidatedInterfaceApi();


?>