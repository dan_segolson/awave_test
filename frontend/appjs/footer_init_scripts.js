//All types of initalisation scripts and snippets goes here

    // Initializations
    $(document).ready(function() {
        //whatever you need to initalize on page load goes here
        
         //table stuff for DataTables:
            $('th').css({'height':'35px'});
            $('td').css({'padding-left':'5px'});
            addrPostsTable = $('#adress_book_table').DataTable( {
                dom: 'B<fl<t>ip>',
                buttons: [ 'copy', 'csv', 'excel' ],
                "language": {
                       "lengthMenu": "Display _MENU_"
                     },
                "lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
                "lengthChange": true,
                "aaSorting": [[2,'asc']] 

            } );
            $('#adress_book_table').addClass('table-responsive table-striped table-bordered');
            $('#adress_book_table').css({'position':'relative', 'margin-left':'0px'});
            $('#adress_book_table > thead > tr > th').css({'font-size':'16px'});
            $('#adress_book_table > tbody > tr > td').css({'font-size':'14px'});
            $('#adress_book_table').on( 'draw.dt', function () {
                $('#adress_book_table > tbody > tr > td').css({'font-size':'14px'});
            });       

    });