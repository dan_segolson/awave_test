
//path to php file for handling of ajax calls
var BASE = window.location.href.split('/').slice(0, 3).join('/');
if(BASE == 'http://localhost'){
    var ADDR_PATH = "/awave_test/controller/addr/";
}else{
    //var BI_PATH = "/payforaccess/controller/bi/";
    var ADDR_PATH = "/controller/addr/"; //if the base path is different on live servers
}

let msg = $('#delete-info-msg');
msg.hide();
msg.removeClass('alert alert-success alert-danger');

//Whatever you need to do over ajax calls

function deleteRow(id){
    msg = $('#delete-info-msg');
    if((id) && (id > 0)){
        let del = confirm('Är du säker på att du vill permanent ta bort kontakten med id '+id+' ? Det går inte att ångra!');
        if(del){
            var param = {id:id};
            $.post(BASE + ADDR_PATH +"addrcalls.php?action=delete", param ,function(response){
            if(response){
                    var reply = JSON.parse(response);
                    //console.log(reply);
                    if(reply.status == 'Kontakten permantent borttagen.'){
                        msg.text(reply.status);
                        msg.addClass('alert alert-success');
                        msg.show();
                        addrPostsTable.row("#"+id).remove().draw();
                    }else{
                        msg.text(reply.error);
                        msg.addClass('alert alert-danger');
                        msg.show();    
                    }                
                }else{
                    msg.text('Inget svar från servern');
                    msg.addClass('alert alert-danger');
                    msg.show();
                }
            });
        }
    }
}







