

//path to process.php for handling of ajax calls
var BASE = window.location.href.split('/').slice(0, 3).join('/');
if(BASE == 'http://localhost'){
    var PATH = "/awave_test/controller/main/"; //use path in your local dev
}else{
    //var PATH = "/payforaccess/controller/main/"; //use path in your local dev
    var PATH = "/controller/main/"; //if the base path is different on live servers
}


//setup so only login is visible at load
$('#register_form').hide();
$('#reg_error_msg').hide();
$('#error_msg').hide();

function showRegister(){
    var login = $('#login_form');
    var register = $('#register_form');
    $('#reg_error_msg').hide();
    login.detach();
    login.insertAfter(register);
    register.show();
    login.hide();
}
function showLogin(){
    var login = $('#login_form');
    var register = $('#register_form');
    $('#error_msg').hide();
    register.detach();
    register.insertAfter(login);
    login.show();
    register.hide();
}


/**
 * handleRegister ensures that the user is a logged in and authorized user in the backend, sets needed sessions etc
 */
function handleRegister(){
    var token = 'boo'; //token comes from external service if you need it
    var new_username = $('#new_username').val();
    var full_name = $('#full_name').val();
    var password_1 = $('#password_1').val();
    var password_2 = $('#password_2').val();
    var msg = $('#pass_compare');
    msg.removeClass('alert alert-success alert-danger');
    msg.hide();
    

    if((new_username != '')
        &&(full_name != '')
        &&(password_1 != '')
        &&(password_2 != '')) {
        if(password_1 != password_2){
            msg.addClass('alert alert-danger');
            msg.text('Passwords not equal');
            msg.show();            
            return false;
        }else{
            var param = {token:token,username:new_username,full_name:full_name,password_1:password_1,password_2:password_2};
                $.post(BASE + PATH +"process.php?action=register", param ,function(response){
                    console.log(response);
                    myData = JSON.parse(response);
                    //console.log(myData);
                if(myData.id > 0){
                    $('#username').val(myData.username);
                    $('#password').val('');
                    showLogin();
                    //window.location = 'home.php'; //to login.php if you need to do something before loading first page
                }else{
                    $('#reg_error_msg').text(response);
                    $('#reg_error_msg').show();
                }
            });
        }
    }else{
        msg.addClass('alert alert-danger');
        msg.text('Missing information in registration form');
        msg.show();            
        return false;
    }
}

/**
 * Used to show passwords in clear text
 * Called from onclick in a check box in the form
 */
function viewPass() {
    var x = $('#password_1');
    var y = $('#password_2');
    //console.log(x.prop('type'));
    if(x.prop('type') === "password") {
        x.prop('type','text');
        y.prop('type','text');
    }else{
        x.prop('type','password');
        y.prop('type','password');
    }
}

/**
 * Clears errormessage whe returning to correct faulty url
 */
function clearMsg(){
    var msg = $('#url_msg');
    msg.text('');
    msg.removeClass('alert alert-success alert-danger');
    msg.hide();
}

/**
 * Basic check to see that the entered url starts with http:// or https:// and ends with /
 * Called onblur="validateURL(this.value)" from the text field holding the url
 * @param  {[type]} textval [the url entered in the text field]
 * @return {[type]}         [true/false]
 */
function validateURL(textval) {
    console.log(textval);
    var msg = $('#url_msg');
    msg.removeClass('alert alert-success alert-danger');
    msg.hide();
    if( ((textval.startsWith("http://"))||(textval.startsWith("https://"))) 
        && (textval.endsWith("/")) 
        ){
        return true;
    }else{
        msg.addClass('alert alert-danger');
        msg.text('Url needs to start with http:// or https:// and end with /');
        msg.show();
        return false;
    }
}

/**
 * Compares two passwords for basic equality
 * called on onclick in the submit-button
 * @return {[type]} [true/false]
 */
function comparePass() {
    var x = $('#password_1');
    var y = $('#password_2');
    var msg = $('#pass_compare');
    msg.removeClass('alert alert-success alert-danger');
    msg.hide();
        
    if(x.val() == y.val()) {
        msg.addClass('alert alert-success');
        msg.text('Passwords match');
        msg.show();
        return true;
    }else{
        msg.addClass('alert alert-danger');
        msg.text('Passwords does not match');
        msg.show();
        return false;
    }
}


/**
 * handleLogin ensures that the user is a logged in and authorized user in the backend, sets needed sessions etc
 */
function handleLogin(){
    var token = 'boo'; //token comes from external service if you need it
        var param = {token:token,username:$('#username').val(),password:$('#pass').val()};
            $.post(BASE + PATH +"process.php?action=login", param ,function(response){
                console.log('response: '+response);
            if(response == 1){
                window.location = 'home.php'; //to login.php if you need to do something before loading first page
            }else{
                //msg.addClass('alert alert-danger');
                $('#error_msg').text(response);
                $('#error_msg').show();
            }
        });

}


/**
 * forceLogout calls the backend to ensure logout in the backend via the API and kill all sessions etc for the user and admin-panel
 * @return {[type]} [description]
 */
function handleLogout() {
    page = '';
    page =  window.location.href.split("/").slice(-1);
       
    $('#error-msg').text('');
    if((page != 'index.php') && (page != '')){
        var param = {token:'-1'};
        //ensure backend is notified and session is killed
        $.post(BASE + PATH + "process.php?action=logout", param ,function(response){
            console.log('response: '+response);
            if(response == 1){
                window.location = 'index.php';
            }else{
                $('#error_msg').text(response);
            }
        });
    }
}
