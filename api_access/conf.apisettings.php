<?php

/**
 * url to external backend api if used
 */


//http/https
define('STAGE_PROTOCOL', 'http://');
define('LIVE_PROTOCOL', 'https://');

if($_SESSION['USE_ENV'] == 'STAGE'){ 
	define('BACKEND_API_URL',STAGE_PROTOCOL.'<domain-here>/api/');
}else{
	//Live - secure api-urls
	define('BACKEND_API_URL',LIVE_PROTOCOL.'<domain-here>/api/');
}

?>