<?php 

/** Managing communication with external rest API backend
*/
/**
 * class ExternalApiManager
 * Base class for curl communication with an external backend from admin application
 *
 * @author Dan Segolson 
 * @version 1.0
 * @since 1.0
 * @access public
 */
 
class ExternalApiManager {


    /** Constructor
    * Initialises the private variables
    */
    var $curl_handle;

    function __construct() {

    }
     
    
    /**
     * xUrl's the api for GET requests
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function getData($data){
        $this->curl_handle = curl_init(); 
        curl_setopt_array(
            $this->curl_handle, array(
            CURLOPT_URL             => $data->sUrl, 
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_HTTPHEADER      => array(
                'Authorization: Bearer '.$data->userToken
                ,'Accept-Language: application/json'
            )             
        ));
        
        $result = $this->curlAPI($this->curl_handle); 
        return $result;
        
    }

    public function postData($data){
        $this->curl_handle = curl_init(); 
        //error_log('POST: '.print_r($data,true));
        curl_setopt_array(
            $this->curl_handle, array(
            CURLOPT_URL             => $data->sUrl, 
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_CUSTOMREQUEST   => 'POST',
            CURLOPT_POSTFIELDS      => $data->sData,
            CURLOPT_HTTPHEADER      => array(
                'Authorization: Bearer '.$data->userToken
                ,'Accept-Language: application/json'
            )
        ));

        $result = $this->curlAPI($this->curl_handle); 
        return $result;
        
    }

    public function putData($data){
        $this->curl_handle = curl_init(); 
        curl_setopt_array(
            $this->curl_handle, array(
            CURLOPT_URL             => $data->sUrl, 
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_CUSTOMREQUEST   => 'PUT',
            CURLOPT_POSTFIELDS      => $data->sData,
            CURLOPT_HTTPHEADER      => array(
                'Authorization: Bearer '.$data->userToken
                ,'Accept-Language: application/json'
            )
        ));

        $result = $this->curlAPI($this->curl_handle); 
        return $result;
        
    }

    public function patchData($data){
        $this->curl_handle = curl_init(); 
        curl_setopt_array(
            $this->curl_handle, array(
            CURLOPT_URL             => $data->sUrl, 
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_CUSTOMREQUEST   => 'PATCH',
            CURLOPT_POSTFIELDS      => $data->sData,
            CURLOPT_HTTPHEADER      => array(
                'Authorization: Bearer '.$data->userToken
                ,'Accept-Language: application/json'
            )
        ));

        $result = $this->curlAPI($this->curl_handle); 
        return $result;
        
    }

    public function deleteData($data){
        $this->curl_handle = curl_init(); 
        curl_setopt_array(
            $this->curl_handle, array(
            CURLOPT_URL             => $data->sUrl, 
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_CUSTOMREQUEST   => 'DELETE',
            CURLOPT_POSTFIELDS      => $data->sData,
            CURLOPT_HTTPHEADER      => array(
                'Authorization: Bearer '.$data->userToken
                ,'Accept-Language: application/json'
            )
        ));

        $result = $this->curlAPI($this->curl_handle); 
        return $result;
        
    }


    private function curlAPI(){
        $output         = curl_exec($this->curl_handle);
        $header_size    = curl_getinfo($this->curl_handle, CURLINFO_HEADER_SIZE);
        $http_response  = curl_getinfo($this->curl_handle, CURLINFO_HTTP_CODE);
        $header         = substr($output, 0, $header_size);
        $body           = json_decode(substr($output, $header_size));      
        
        debug::handle_error("\r\n API response: ".$http_response." output: ".print_r($output,true)."\r\n");

        if( ($http_response == '401' || $http_response == '403') ){
            debug::handle_error("\r\n response: ".$http_response." error-info: ".print_r($output,true)."\r\n");
        }
        curl_close($this->curl_handle);
        return $output;

    }
     
     
}

?>