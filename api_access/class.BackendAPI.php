<?php
require_once(DIR_APPLICATION.'api_access/class.ExternalApiManager.php');
require_once(DIR_APPLICATION.'api_access/conf.apisettings.php');


/** 
* This contains all methods we will call in the backend API, regardless if it is over a REST api or direct db calls.
* response is always in json so model does not have to differ between API responses or DB
* apisettings contains all conf variables to connect to the service api
* Each method takes needed parameters, inserts in predefined urls for GET calls, prepares payload for POST/PUT/DELETE calls, or it adds them to parameterized queries depending on call.
*/
class BackendApi extends ExternalApiManager {


	var $sApiEndPoint;
	var $sApiEndPoint2;
	var $sApiKey;
	var $key;
	var $sClientId;


	function __construct($token = '',$api = ''){
		parent::__construct();

		//sample
		if($api != ''){
			$this->sApiEndPoint = $api ;
		}else{
			$this->sApiEndPoint = '';
		}
		
		//auth token in case it needs to be sent in header
		$this->userToken = $token;
	}


/** DB calls */
	/**
	 *  
	 * Gets  user from id
	 * @return JSON     array with one represenatation of the user 
	 */
	public function db_getUserByNick($data){
		
		if(strlen($data->sUsername) > 2){
			$sSQL = sprintf("SELECT u.*   
							FROM users u  
							WHERE u.username = '%s' 
							ORDER BY u.id ASC", $data->sUsername);
			$data = $this->query($sSQL);
			$data = $this->latin1ToUtf8($data);
			$data = json_encode($data);
			return $data;
		}

	}

	/**
	 *  
	 * Gets all users from id
	 * @return JSON     array 
	 */
	public function db_getUsers(){
		
			$sSQL = "SELECT u.* , r.role as user_role   
						FROM users u 
						LEFT JOIN roles r ON r.id = u.role 
						ORDER BY u.id ASC";
			$data = $this->query($sSQL);
			$data = $this->latin1ToUtf8($data);
			$data = json_encode($data);
			return $data;

	}

	/**
	 *  
	 * Gets  user from id
	 * @return JSON     array with one represenatation of the user
	 */
	public function db_getUserById($data){
		
		if($data->user_id != ''){
			$sSQL = sprintf("SELECT u.*   
							FROM users u 
							WHERE u.id = '%s' 
							ORDER BY u.id ASC", $data->user_id);
			$data = $this->query($sSQL);
			$data = $this->latin1ToUtf8($data);
			$data = json_encode($data);
			return $data;
		}

	}

	

	/**
	 * Registers user in DB
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function db_registerUser($data){
		$created = date('Y-m-d H:i:s');
		if(($data->username != '')&&($data->password != '')){
			
			$sSQL = sprintf("INSERT INTO users 
				(
				username,
				full_name,
				password,
				is_logged_in,
				created
				) 
				VALUES 
				('%s' ,'%s' ,'%s' ,'%s','%s')"
				, $data->username,$data->full_name,$data->password,'0',$created);

			$id = $this->insert($sSQL);
			if($id > 0){
				$data = '{"id":"'.$id.'"}';	
			}else{
				$data = '{"id":"0", "error":"Insert failed"}';	
			} 
			return $data;
		}
		
	}



	public function db_updateLoginStatus($param){

		$response = isset($response) ? $response : new stdClass();

		$last_login = date('Y-m-d H:i:s');
		$logged_in = $param->login_status;;
		$login_token = $param->login_token;
		$user_id = $param->user_id;		

		if(($logged_in != '') && ($user_id != '')){
			if($logged_in == 0){
				$sSQL = sprintf("UPDATE users 
							SET 
							is_logged_in = '%s',
							authentication_token = '%s'
							WHERE id = '%s' ",$logged_in,$login_token,$user_id);	
			}else{
				$sSQL = sprintf("UPDATE users 
							SET 
							last_login = '%s',
							is_logged_in = '%s',
							authentication_token = '%s'
							WHERE id = '%s' ",$last_login,$logged_in,$login_token,$user_id);
			}
		}

		$nr_rows = $this->update($sSQL);
		if($nr_rows > 0){
			$response->status = 'Updated';
		}else{
			$response->error = 'Not updated';
		}
		return $response;
		
	}


	/**
	 *  
	 * Gets adress book for user from key
	 * @return JSON     adresse data if found
	 */
	public function db_searchAddressBooks($data){
		if(strlen($data->search) > 2){

			if($data->owner_id != ''){
				$sSQL = sprintf("SELECT ab.* , u.username 
								FROM adress_books ab  
								LEFT JOIN users u ON u.id = ab.owner_id 
								WHERE ab.full_name LIKE '%%%s%%' 
								AND ab.owner_id = '%s' 
								ORDER BY ab.id ASC", $data->search,$data->owner_id);
			}else{
				$sSQL = sprintf("SELECT ab.* , u.username
								FROM adress_books ab  
								LEFT JOIN users u ON u.id = ab.owner_id 
								WHERE ab.full_name LIKE '%%%s%%' 
								ORDER BY ab.id ASC", $data->search,$data->search);
			}
			$data = $this->query($sSQL);
			$data = $this->latin1ToUtf8($data);
			$data = json_encode($data);
			return $data;
		}

	}

	/**
	 *  
	 * Gets adress book for user from key
	 * @return JSON     adresse data if found
	 */
	public function db_getAdressBookByOwner($data){
		if($data->owner_id != ''){
			$sSQL = sprintf("SELECT ab.* , u.username  
							FROM adress_books ab  
							LEFT JOIN users u ON u.id = ab.owner_id 
							WHERE ab.owner_id = '%s' 
							ORDER BY ab.id ASC", $data->owner_id);
			$data = $this->query($sSQL);
			$data = $this->latin1ToUtf8($data);
			$data = json_encode($data);
			return $data;
		}

	}


	

	/**
	 *  
	 * Gets all adressbooks
	 * @return JSON   data if found
	 */
	public function db_getAdressBooks(){
			$sSQL = "SELECT ab.*, u.username from adress_books ab 
					LEFT JOIN users u ON u.id = ab.owner_id 
					order by ab.owner_id ASC, ab.full_name";
			$data = $this->query($sSQL);
			$data = $this->latin1ToUtf8($data);
			$data = json_encode($data);
			return $data;

	}

	public function db_insertContact($data){
		if($data->owner_id != ''){
			$created = date('Y-m-d H:i:s');
			$sSQL = sprintf("INSERT INTO adress_books 
				(
				full_name,
				email,
				image_url,
				birth_date,
				owner_id,
				created
				) 
				VALUES 
				('%s' ,'%s' ,'%s' ,'%s','%s','%s')"
				, $data->full_name,$data->email,$data->image_url,$data->birth_date, $data->owner_id,$created);

			$id = $this->insert($sSQL);
			if($id > 0){
				$data = '{"id":"'.$id.'"}';	
			}else{
				$data = '{"id":"0", "error":"Kontakt kunde ej skapas"}';	
			} 
			return $data;
		}
		
	}

	/**
	 *  
	 * Gets contact from id
	 * @return JSON     array with one represenatation of the user
	 */
	public function db_getContactById($data){
		
		if($data->id != ''){
			$sSQL = sprintf("SELECT ab.*   
							FROM adress_books ab 
							WHERE ab.id = '%s' 
							ORDER BY ab.id ASC", $data->id);
			$data = $this->query($sSQL);
			$data = $this->latin1ToUtf8($data);
			$data = json_encode($data);
			return $data;
		}

	}

	public function db_updateContactById($data){

		if($data->id != ''){
			$sSQL = sprintf("UPDATE adress_books  
						SET 
						full_name = '%s',
						email = '%s',
						image_url = '%s', 
						birth_date = '%s', 
						owner_id = '%s'
						WHERE id = '%s' ",$data->full_name,$data->email,$data->image_url,$data->birth_date,$data->owner_id, $data->id);
				

			$nr_rows = $this->update($sSQL);
			if($nr_rows > 0){
				$response = '{"status":"Uppdaterad"}';
			}else{
				$response = '{"error":"Ej updaterad"}';
			}
			return $response;
		}
		
	}


	public function db_updateImageUrl($data){

		if($data->id != ''){
			$sSQL = sprintf("UPDATE adress_books  
						SET 
						image_url = '%s'
						WHERE id = '%s' ",$data->image_url,$data->id);
					
			$nr_rows = $this->update($sSQL);
			if($nr_rows > 0){
				$response = '{"status":"Uppdaterad"}';
			}else{
				$response = '{"error":"Ej updaterad"}';
			}
			return $response;
		}
		
	}

	public function db_deleteContactById($data){
		if($data->id > 0){
			$sSQL = sprintf("DELETE FROM adress_books 
					WHERE id = '%s' "
					, $data->id);

			$rows = $this->delete($sSQL);
			if($rows > 0){
				$data = '{"rows":"'.$rows.'"}';	
			}else{
				$data = '{"rows":"0", "error":"Kontakten kunde ej tas bort"}';	
			} 
			return $data;
		}
		
	}

/** SAMPLE for using a REST API as a backend instead of a DB */
	/**
	 * api_login POST
	 * Called from the view OR via API request ->controller->model per usual. 
	 * Logs in a user in a API-based backend via its Bearer token passed in the constructor.
	 * Requires the endpoints etc to be setup in conf.apisettings.php
	 * @return JSON     user logged in or not - plus the full userobject on success
	 */
	/*public function api_login($data){
		$aData = array(
			'sAppToken' => $data->sAppToken
            ,'sUsername' => $data->sUsername
            ,'sPassword' => $data->sPassword
		);
    	$sData = json_encode($aData);

		$sUrl = $this->login;
		$params = (object) array(
			'sUrl' => $this->sApiEndPoint.$sUrl
			,'userToken' => $this->userToken
			,'sData' => $sData				
			);
		$result = $this->postData($params);
		$tmp = json_decode($result);

		if($tmp->iLoginStatus == 'True'){
			$retVal = '{"user":'.$result.',"status": "LoggedIn","header":"200"}';	
		}else{
			$retVal = '{"error":"NotAuthorised","header":401}';
		}
		return $retVal;
	}*/

/** END SAMPLE for using a REST API as a backend instead of a DB */


/** START helper for json issues */
	/**
	 * Encode array from latin1 to utf8 recursively
	 * @param $dat
	 * @return array|string
	 */
	   private static function latin1ToUtf8($dat)
	   {
	      if (is_string($dat)) {
	         return utf8_encode($dat);
	      } elseif (is_array($dat)) {
	         $ret = [];
	         foreach ($dat as $i => $d) $ret[ $i ] = self::latin1ToUtf8($d);

	         return $ret;
	      } elseif (is_object($dat)) {
	         foreach ($dat as $i => $d) $dat->$i = self::latin1ToUtf8($d);

	         return $dat;
	      } else {
	         return $dat;
	      }
	   }

/** 
 * START direct db access for missing APIs calls
 * Uses PDO mysql
 */

	private function getDB(){
		
		if($_SESSION['USE_ENV'] == 'STAGE'){ 
			$host = DEV_DATABASE_URL;
			$port = DEV_DATABASE_PORT;
			$db_name = DEV_DATABASE_NAME;
			$db_user = DEV_DATABASE_USER;
			$db_pass = DEV_DATABASE_PASS;

			return new PDO("mysql:host=$host;dbname=$db_name", $db_user, $db_pass);
		}else{
			$host = LIVE_DATABASE_URL;
			$port = LIVE_DATABASE_PORT;
			$db_name = LIVE_DATABASE_NAME;
			$db_user = LIVE_DATABASE_USER;
			$db_pass = LIVE_DATABASE_PASS;	

			return new PDO("mysql:host=$host;dbname=$db_name", $db_user, $db_pass);
		}

	}

	private function query($sSQL){
		debug::handle_error("db_query: ". $sSQL);
		$aRetVal = array();
		$db = $this->getDB();
		$statement = $db->prepare($sSQL);
		$statement->execute();
		while ($obj = $statement->fetchObject()) {
			$aRetVal['data'][] = $obj;    
		}
		return (object)$aRetVal;
	}

	private function insert($sSQL){
		debug::handle_error("insert: ". $sSQL);
		$db = $this->getDB();
		$statement = $db->prepare($sSQL);
		$statement->execute();
		
        return $db->lastInsertId($seq);
	}

	private function update($sSQL){
		debug::handle_error("update: ". $sSQL);
		$db = $this->getDB();
		$statement = $db->prepare($sSQL);
		$statement->execute();
		// return the number of row affected
        return $statement->rowCount();
	}

	private function delete($sSQL){
		debug::handle_error("delete: ". $sSQL);
		$db = $this->getDB();
		$statement = $db->prepare($sSQL);
		$statement->execute();
		// return the number of row affected
        return $statement->rowCount();
	}


}

?>