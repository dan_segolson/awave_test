# Awave PHP developer Work test

Setup the site
==============
This is built in PHP7 and MySQL 5.7.23 using my own PHP boilerplate to quickly get a basic standard site up and running.

1. Create a mysql-db called "awave_test" (or whatever you like)
2. Run the sql-dump you find in the root ( awave_test_2019-02-28.sql ) to seed the db with some data.
3. Go into the file conf/local.conf.php and update the defines for  LIVE_DATABASE_xxx and DEV_DATABASE_xxx and enter your db-name, user and password (there are two different sets of DB defines since my boilerplate allows to set it up so you can switch between say a stage-db and a live-db) Default used is LIVE.
4. The site expects to be located at server-root/awave_test/ so you can reach it through http://localhost/awave_test/ 
5. http://localhost/awave_test/ should now get you a login screen with the possibillity to register a new user.
6. To log in with the pre-registered administrator, user: admin and pass: admin
7. To register a new user, switch to register and ... do it :)

If you get this far, all should be good, if not, please contact me at the mail/phone nr you have available.

Note that I will make this repo private again as soon as I know that you have cloned it and gotten it up and running. So please let me know.


Used time for the delivery
==========================
Due to using my boilerplate I got a lot "for free", that's why it's quite a bit more than stated in the requirements (plus it's kind of fun to do it well).

Approximate develpopment time was around 16 hours to setup the site, the db and code the actual address book features + perhaps 3-4 hours setting up repo, creating documentation, cleaning up the code and testing the installation instructions.


Requirements
========
The requirements stated

 - Admin should be able to show, add, edit and delete contacts
 - User should be able to login with username
 - When user is logged in, he should see a paginated table view over his contacts in the address book
 - User should be able to add a contact
 - Each contact should have name, email, image, date of birth
 - A Cron job should be able to trigger a happy birthday mail to contacts
 - Extra - A simple REST-API to list and create contacts in adress book.


Implemented features
====================
 - System has an admin pre-registered user: admin , pass: admin 
 - You can login and register a user. The registered user defaults to normal user, I have not implemented changing role, that has to be done in the db for now.
  - Passwords are one-way encrypted using bcrypt, login sets a session with an auth token which times out on inactivity.
 - As logged in user you can view/edit/create/delete your own contacts.
 - As logged in admin you can view/edit/create/delete all contacts.
 - Each registered user creates his own address book as he creates his first contact.
 - You can search for contact with part of name, or view all contacts, in the paginated result you can filter, sort and export results.
  - As you add/edit contacts, you can upload an image which is resized and thumb is used for profile image. Original image is also saved.
 	 - Uploaded images are saved in a folder called uploads with its own structure and referred to with a full URL, this makes it perectly possible to upload to an AWS bucket or CDN if needed.
  - Happy birthday mails is sent through calling an endpoint which runs a job, this triggers the backend to grab all contacts, compare birthday (month/day) to todays month/day, if it's a match, php's built in mail-client sends a happy birthday mail to the user, greeting him/her by name
   - A REST-API is setup to export all contacts (GET), get all contacts for an adress-book-owner (GET), edit a contact by id (PATCH), add a contact to a users adress-book(POST), delete a contact by id (DELETE).
   - "pretty urls" is implemented acreoss the site, an .htaccess file for rewrites is included in the frontend directly if you use an Apache webserver. For nginx a snippet is provided in the section "STRUCTURE"

Not, or partly, implemented
=====================
 - I have not implemented changing role for a user, that has to be done in the db for now.
 - No Form validations in JS or on the server for things like properly formed emails, birth-dates etc. I basically only check in HTML if upload is an image.
 - No CSRF tokens or nonce to prevent double submits of forms or Cross Site Request Forgery attacks
 - Partly used exception handling in the cron job for mail().
 - Partly used own error_handler for debugging etc, creates a full backtrace in logs if you use debug::handle_error() instead of standard error_log(). Can also be globally turned off in the class.error_handler.php
 - No Unit tests are implemented
 - DB is a bit normalized to keep role definitions apart from the users and has FK's to enforce role connections and owners of contacts



CRON-jobs
=========
Since the actual running of the cron-jobs are on the server, not in php, I created an endpoint where cron can call different methods for different tasks. Implemented is the birthdayJob. 

 - To test it, ensure you have a contact with birth date "today" (YYYY-MM-DD), then call http://localhost/awave_test/frontend/cron.php?method=birthdayJob if your server is setup to allow PHP to send mails via mail(), that user will be greeted (it will end up in spambox due to localhost sending etc).
  - NOTE: With large volumes of contacts, it would be better to have this as a file that cron can run using the CLI version of php to prevent bottlenecks.
 - The birthdayJob uses exception handling around the mail() to catch and log errors.



REST-API
========
The API supports:

 - export of all contacts from all address books
 - export of a single users address book
 - add contact in a specific users adress book
 - edit contact by id
 - delete contact by id

NOTE:

 - I have not implemented image upload over the API at this point, but with some modification of incoming data in the model, it is possible to recieve base64encoded images, process them and upload to a contact.
 - I have not implemented oAuth for user identification in API which it probably should have for user role based access.

 When testing, use " Authorization: Bearer awave_test " as token in the API.

A Postman collection in json (as well as an image of generated documentation which I could not share) can be found in the root of the project: api-tests.postman_collection.json



STRUCTURE
=========
The boilerplate core information:
IMPORTANT:

 Core structure:

  - /api_access/
   - conf.apisettings -> constants used for access, url's token etc etc
   - class.ExternalApiManager.php -> holds all cURL code to create proper calls from this to an external API and managing responses supporting GET/POST/PUT/PATCH/DELETE with auth-keys in header.
   - class.BackendAPI.php -> extends the ExternalApiManager and holds the actual methods each page the admin panel will use for retrieving and sending data. General returned data structure from the database should be the same as from the API's to simplify in the model

  - /conf/ -> Holds conf files for all navigation links, creation of constants for include paths, inclusion of core files etc. Here you will also find a simple router based on a switch that tests the GET request and selects ctrl page and template for the request. 
  (This could simply be done by just using the passed GET['page'] variable for selections, but using the router switch we ensure that only allowed pages are reached this way)

  - /model/ -> holds a main class 'AdminModel' containing the methods which will apply business logic to request data and process responses to the views/templates. In most cases this does very little, but if you for example need to merge info from two data sources and joins are m´not possible, this is where it will happen. Also contains a class creating an interface to managing the login sessions. bi_KPI class for BI KPI threashold calculations, fileManager and a function for resizing images.
  
    - /model/api/ -> Here we have a simple rest-server class and an endpoint class which will connect to the model. The endpoint class hold the external endpoints which can be used to retrieve content via a REST API.
    Actual api-interface that you call with method and payload is located here: 
    /frontend/api.php

  - /controllers/ -> There is one base controller handling things all pages basic needs, like access to model, sessions methods for login/auth/logout etc. Each page has it's own ctrl.<pagename>.php file which includes the base controller, conf etc, this retrieves links for the page and/or its navigation. It also pulls in its own controller which extends the base, this looks for passed actions and data to preprocess things adccording to requested task so it can either be passed to the model and from there to the API, or process the responses for presentation in the page.

  - /frontend/templates/ -> holds the html sections each page will use, these will only hold enough php for presentation based on the info coming from the pages controller.

  - /frontend/home.php -> this is the main page once logged in, this renders the selected ctrl pages and templates from the router.

  - /frontend/index.php -> exactly what it looks like, where you login and where you end up after logout.

  - /frontend/login.php -> A middle page between index and home, probably not essential (it looks damn good though) but it ensures that all session cookies are properly set and accessable on the next page.

   - /frontend/api.php -> accesspoint to grab content as JSON for interface generation in any client.

  - /frontend/appjs/ holds application specific javascript such as general initialisations and specific ajax calls etc for different pages

  - /frontend/css, font, img, js, are mainly the files used to override cdn-based core css/js. The framework used is visually based on Bootstrap4 and flex + jQuery


  .htaccess for pretty urls is included in frontend folder
  If you are running ngnix, add this to the ngnix conf file, adjusted for paths etc of course:

   - pretty urls rewrite for nginx, add to live conf with right paths
    location ~* /aftermath_engagement_engine/frontend/ {
        rewrite page/(.*)/?$ /aftermath_engagement_engine/frontend/home.php?page=$1 break;
        rewrite web-api/v1/(.*)/?$ /aftermath_engagement_engine/frontend/web-api.php?method=$1 break;
        fastcgi_pass     unix:/Applications/MAMP/Library/logs/fastcgi/nginxFastCGI.sock;
      fastcgi_param    SCRIPT_FILENAME $document_root$fastcgi_script_name;
      include          fastcgi_params;
    }
 - end pretty urls rewrite for nginx, add to live/dev server conf with right paths

