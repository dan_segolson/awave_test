<?php

 /** This is a class to create a file intended for export of data. 
 * Offers three main methods that allows you to either write a line word by word
 * or to write the file line by line. Then you write the actual file and close it.
 * @author Dan Segolson { dms@dmsproject.com }
 * @version 0.1
 * @since 0.1
 * @access public
*/
class cMakeFile{
/** Internal variables
* holds the initially passed filepath, filename, a timestamp, a handle to the created file
* a buffer that holds whatever you write to the file plus a return value.
*/
    var $_filepath;
    var $_filename;
    var $_file;
    var $_now;
    var $_handle;
    var $_buffer;
    var $_retVal;

/** Constructor. Takes a path, filename plus an extension as parameters
* It defaults to a folder under the class location called export, file name defaults to
* codeExport, the extension defaults to .txt. If a filename is passed it is checked for invalid chars
* $param STRING $path, STRING $file, STRING $ext
*/
    function __construct($path,$file,$ext){
        if($path ==""){
            $path = DIR_APPLICATION.GDPR_EXPORT_PATH;
        }
        if($file==""){
            $file = "codeExport";
        }else{
            $rX = "/[å+ä+ö+Å+Ä+Ö+\ +]/";
           $file = preg_replace($rX,"_",$file);
        }
        if($ext==""){
            $ext = ".txt";
        }
        /**
        * Calls an internal method to create a timestamp
        * Builds a complete filename incl path
        * Calls internal method createFile()
        */
        $this->timeStamp();
        $this->_filepath = $path;
        $this->_filename = $this->_now."_".$file.$ext;
        $this->_file = $this->_filepath.$this->_filename;
        $this->createFile();
    }
    /**
    * Creates a YmdHis timestamp used to make the filename unique
    */
    function timeStamp(){
        $this->_now = date("YmdHis");
    }
    /**
    * Creates a handle to a file
    */
    function createFile(){
        $this->_handle = fopen($this->_file,"a b");
    }
    /**
    * Writes a string to a line in the buffer for the file
    * @param STRING $line
    */
    function writeString($line){
        $this->_buffer = $this->_buffer.$line;
    }
    /**
    * Writes a complete line in the buffer for the file and adds a linefeed
    * @param STRING $line
    */
    function writeLine($line){
        $this->_buffer = $this->_buffer.$line."\r";
    }
    /**
    * Writes the contents of the buffer to the actual file
    * Then closes the file
    * @param STRING $line
    * @return STRING Message if it went well or not.
    */
    function writeFile(){
        if(!fwrite($this->_handle,$this->_buffer)){
            $this->closeFile();
            $this->_retVal = "Cannot create file ".$this->_file;
            return $this->_retVal;
            
        }else{
            $this->closeFile();
            $this->_retVal = $this->_file;
            return $this->_retVal;
        }
    }
    /**
    * Chmods and Closes the created file
    *
    */
    function closeFile(){
	   @chmod($DOCUMENT_ROOT.$REQUEST_URI.'/'.$this->_file,777);
        fclose($this->_handle);
    }

}

?>
