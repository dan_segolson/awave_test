<?php  


/** The simples mail class ever... 
* Normally you would use something like Mandrill to send templated event driven mails.
*/

class Mailer {

	function __construct(){

	}


	public function sendMail($data){
		$message = "Jag vill passa på att gratulera dig på födelsedagen!\nHoppas du får en underbar dag :) ";
		$message = wordwrap($message,70);
		$data->msg = $message;
		if($this->checkBirthDay($data)){
			$this->send($data);	
		}
		
	}

	private function send($data){
		// actually send emailto the ones with a birthday today
		try{
			mail($data->email,'Grattis '.$data->full_name, $data->msg);	
		}catch(MailerException $e){
			debug::handle_error($e);
			return false;
		}
		
	}


	private function checkBirthDay($data){
		$today = date('m-d');
		$month = date('m',strtotime($data->birth_date));
		$day = date('d',strtotime($data->birth_date));
		$birthdate = $month.'-'.$day;
		if($today === $birthdate){
			return true;
		}

	}
}


?>