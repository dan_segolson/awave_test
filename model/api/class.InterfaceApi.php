<?php
require_once("class.RestServer.php");


/** 
* Core api-interface for the backend application.
* The package contains all needed api functionality to enabling creation a new user-interface in any language which supports a REST api speaking json in both directions
* @package CoreAPIPackage
* @author dms@dmsproject.com
* @version 1.0
* @since 1.0
* @access public
*/
/**
 *  class InterfaceApi
 * Base api-class for data management between thin interfaces of various cases and the application logic
 *
 * @author dms@dmsproject.com
 * @version 1.0
 * @since 1.0
 * @access public
 */

    
// Configuration



class InterfaceApi
{
    
// set to false since we default to the api used internally
// if api is to be used by external client they would call the extension class 
// to override validate to true and trigger the timeout validation.
    public $validate;
    

    public function __construct(){   
        
        date_default_timezone_set('UTC');
        //validate api calls, false if used as include. External interface resets this to true if called from device etc
        $this->validate = false;

        
    }

    /**
     * To be called after instantiation if you want validation on
     */
    protected function setValidate() {
        $this->validate = true;
    }

    /** 
    * function cleaner() 
    * @access private 
    * @param  array of various strings to be cleaned
    * @note Removes characters/entities which can screw up json encoding.
    * @note As these entities are removed from the content stored in db, this will not be needed
    */
    private function cleaner($aData) {
        $aSearch = array('<br>');
        $sReplace = '' ;
        foreach($aData[0] as $key => $string) {
            if($string != ''){
                //$aTmp[$key] = preg_replace( "/\r|\n/", "", $string );
                $aTmp[$key] = str_replace($aSearch,$sReplace,$string);
            }
        }
        return $aTmp;
    }

    private function getAuthToken(){
        if (!function_exists('getallheaders')) { 
            function getallheaders() { 
               $headers = []; 
               foreach ($_SERVER as $name => $value) { 
                   if (substr($name, 0, 5) == 'HTTP_') { 
                       $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
                   } 
               } 
               return $headers;
            } 
        }


        $headers = getallheaders();
        //debug::handle_error($headers);
        return str_replace('Bearer ','',$headers['Authorization']);

    }

    /**
     * Temporary validation of external token used by approved apps who can use the API
     * @param  STRING $token
     * @return BOOL
     */
    private function validateApp($token) {
        //debug::handle_error($token);
        if($this->validate == true){
            $oModel = new AdminModel();
            $data = new stdClass();
            $data->token = $token; 

            $aOkToken = array('awave_test');       
            if(in_array($token,$aOkToken)){
                debug::handle_error('validateApp ok, token: '. $token);
                return true;
            }else{
                //log call w/o valid app token in db
                debug::handle_error('validateApp fail ');
                return false; 
            }
        }else{
            return true;
        }
    }

/***************************/

    /**
    *  @api {GET} export_contacts export_contacts
    * @apiName export_contacts
    * @apiGroup Contact
    * @apiParamExample 
    * api/v1/export_contacts
    * 
    * @apiDescription Retrieves all contacts for all owners
    *
    *
     * @apiSuccessExample {json} Success-Response::
    * HTTP/1.1 200 
        {
        "data": [
            {
                "id": "2",
                "full_name": "Daniela Karlsson",
                "email": "d.msproject@gmail.com",
                "image_url": "http://localhost/awave_test/uploads/th/th_2.jpg",
                "birth_date": "1948-12-22",
                "owner_id": "2",
                "created": "2019-02-26 16:02:38",
                "username": "admin"
            }
        ]
        }
    *
    * @apiErrorExample {json} Error-Response::
    * HTTP/1.1 202 Accepted 
        {
        "id": "0",
        "error": "No contacts found"
        "header": "202"
        }
    *
    */
    public function export_contacts(){
        if( $this->validateApp($this->getAuthToken()) ){ 
            $oModel = new AdminModel();
            $data = new stdClass();
            $ret = $oModel->getAddressBooks($data);
            debug::handle_error($ret);
            if($ret->data){
                $response = $ret;
            }else{
                $response = json_decode('{"error":"No contacts found","header":"202"}');
            }
            return $response;
        }

   } 

    /**
    *  @api {GET} get_contacts_for_id get_contacts_for_id
    * @apiName get_contacts_for_id
    * @apiGroup Contact
    * @apiParamExample 
    * api/v1/get_contacts?id={id}
    * 
    * @apiDescription Retrieves all contacts for an owner
    *
    *
     * @apiSuccessExample {json} Success-Response::
    * HTTP/1.1 200 
        {
        "data": [
            {
                "id": "2",
                "full_name": "Daniela Karlsson",
                "email": "d.msproject@gmail.com",
                "image_url": "http://localhost/awave_test/uploads/th/th_2.jpg",
                "birth_date": "1948-12-22",
                "owner_id": "2",
                "created": "2019-02-26 16:02:38",
                "username": "admin"
            }
        ]
        }
    *
    * @apiErrorExample {json} Error-Response::
    * HTTP/1.1 202 Accepted 
        {
        "id": "0",
        "error": "No contacts found"
        "header": "202"
        }
    *
    */
    public function get_contacts_for_id($id){
        if( $this->validateApp($this->getAuthToken()) ){ 
            $oModel = new AdminModel();
            $data = new stdClass();
            $data->owner_id = $id;
            $ret = $oModel->getAddressBookByOwner($data);
            debug::handle_error($ret);
            if($ret->data){
                $response = $ret;
            }else{
                $response = json_decode('{"error":"No contacts found","header":"202"}');
            }
            return $response;
        }

   }  

    
    /**
    *  @api {POST} add_contact add_contact
    * @apiName add_contact
    * @apiGroup Contact
    * @apiParamExample {json} Request-Example:
    * {
    *    'full_name' : <STRING>
    *    ,'email' : <STRING>
    *    ,'birth_date' : <STRING>
    *    ,'owner_id' : <INT>
    *    
    * }
    * @apiDescription Takes a contact (except for the image) and adds it in the db
    *
    *
     * @apiSuccessExample {json} Success-Response::
    * HTTP/1.1 201 Created 
        {
        "id": "12",
        "status": "Contact added",
        "header": "201"
        }
    *
    * @apiErrorExample {json} Error-Response::
    * HTTP/1.1 202 Accepted 
        {
        "error": "Contact not added"
        "header": "202"
        }
    *
    */
    public function add_contact($data){
        if( $this->validateApp($this->getAuthToken()) ){ 
           $oModel = new AdminModel();
            $ret = $oModel->addContactForId($data);
            debug::handle_error($ret);
            if($ret->id > 1){
                $response = json_decode('{"id":"'.$ret->id.'", "status":"Contact added","header":"201"}');
            }else{
                $response = json_decode('{"error":"Contact not added","header":"202"}');
            }
            return $response;
        }
   }  

   /**
    *  @api {PATCH} edit_contact edit_contact
    * @apiName edit_contact
    * @apiGroup Contact
    * @apiParamExample {json} Request-Example:
    * {
    *    'full_name' : <STRING>
    *    ,'email' : <STRING>
    *    ,'birth_date' : <STRING>
    *    ,'owner_id' : <INT>
    *    
    * }
    * @apiDescription Takes a contact (except for the image) and adds it in the db
    *
    *
     * @apiSuccessExample {json} Success-Response::
    * HTTP/1.1 201 Updated 
        {
        "id": "12",
        "status": "Contact edited",
        "header": "201"
        }
    *
    * @apiErrorExample {json} Error-Response::
    * HTTP/1.1 202 Accepted 
        {
        "id": "0",
        "error": "Contact not added"
        "header": "202"
        }
    *
    */
    public function edit_contact($data){
        if( $this->validateApp($this->getAuthToken()) ){ 
           $oModel = new AdminModel();
            $ret = $oModel->updateContactById($data);
            debug::handle_error($ret);
            if($ret->id > 1){
                $response = json_decode('{"id":"'.$id.'", "status":"Contact edited","header":"201"}');
            }else{
                $response = json_decode('{"error":"Contact not edited","header":"202"}');
            }
            return $response;
        }
   }  
    

    /**
    *  @api {DELETE} delete_contact delete_contact
    * @apiName delete_contact
    * @apiGroup Contact
    * @apiParamExample {json} Request-Example:
    * {
    *    'id' : <INT>
    * }
    * @apiDescription Deletes a contact by id
    *
    *
     * @apiSuccessExample {json} Success-Response::
    * HTTP/1.1 201 Deleted 
        {
        "id": "12",
        "status": "Contact deleted",
        "header": "201"
        }
    *
    * @apiErrorExample {json} Error-Response::
    * HTTP/1.1 202 Accepted 
        {
        "id": "0",
        "error": "Contact not deleted"
        "header": "202"
        }
    *
    */
    public function delete_contact($data){
        if( $this->validateApp($this->getAuthToken()) ){ 
           $oModel = new AdminModel();
            $ret = $oModel->deleteContact($data);
            debug::handle_error($ret);
            if($ret->rows > 0){
                $response = json_decode('{"status":"Contact deleted","header":"201"}');
            }else{
                $response = json_decode('{"error":"Contact not deleted","header":"202"}');
            }
            return $response;
        }
   }  


}
