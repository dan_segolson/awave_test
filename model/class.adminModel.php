<?php
require_once(DIR_APPLICATION.'api_access/class.BackendAPI.php');
//require_once('tools/class.cMakeFile.php');
require_once('helpers/class.cEncryptorInterface.php');


/**
 * AdminModel
 * Recieves data from controllers, applies various logic and passes it on to the BackendAPI
 * Recieves responses from BackendAPI, processes it for business logic and returns to the controller.
 */
	class AdminModel {
		var $oSession;
		var $oEncryptor;

		/**
		 * Initializes an instance of a session interface
		 */
		function __construct(){
			$this->oSession = new SessionInterface();
			$this->oEncryptor = new cEncryptorInterface();
		}


/** User section */
		/**
		 * Performs auth against the session
		 * Called from controller on a per pageload auth
		 * Validates if there is an authenticated session, if not, it calls logout 
		 * @note - subject for redesign once we have the real API up
		 * @return BOOL  true/false
		 */
		public function auth() {
			$sess = $this->oSession->getSession('login');
			$response = (object)$sess['login'];
			if($response->status == 'Authenticated'){
				return true;
			}else{
				$this->backend_logout();
				return false;
			}
			
		}


		/**
		 * Backend login method  
		 * Logs in against backend db
		 * @param  [type] $param [ ->sUsername, ->sPassword]
		 * @return [type]        [description]
		 */
		public function backend_login($param){
			$data = isset($data) ? $data : new stdClass();
			$oApi = new BackendApi($param->token);
			$oValidated = $this->validateUserPass($param);
			if($oValidated->validated == 1){
				$sNewToken = $this->oEncryptor->getNewToken();
				$data->user_id = $oValidated->user_id;
				$data->login_token = $sNewToken;
				$data->login_status = '1';
				$response = $oApi->db_updateLoginStatus($data);
				
			}else{
				$response = isset($response) ? $response : new stdClass();
				$response->error = 'Not Updated';
			}
			
			if($response->status == 'Updated'){
				$response = $oValidated->user;
				return $response;
			}else{
				$response->error;
			}
		}

		/**
		 * Backend logout method  
		 * Logs out against backend db
		 * @return [type]        [description]
		 */
		public function backend_logout(){
			$data = isset($data) ? $data : new stdClass();
			$oApi = new BackendApi();
			$sess = $this->oSession->getSession('login');

			$data->user_id = $sess['login']['id'];
			$data->login_token = $sess['login']['login_token'];
			$data->login_status = '0';
			
			$response = $oApi->db_updateLoginStatus($data);
			if($response->status == 'Updated'){
				return 1;
			}else{
				return 0;
			}
		}

		/**
		Register user, each entry in the user adressbooks will later be tagged with owner = user_id.
		 * @param  [type] $data [description]
		 * @return [type]       [description]
		 */
		public function backend_registerUser($data){
			$oApi = new BackendAPI($param->token);
			$param = new stdClass();
			//encrypt password
			$param->password = $this->oEncryptor->hashUserPass($data->password);
			$param->username = $data->username;
			$param->full_name = $data->full_name;
			
			//reg user
			$response = json_decode($oApi->db_registerUser($param));
			if($response->id > 0){
				$response->username = $param->username;
				return $response;
			}else{
				return 0;
			}
			
			if($response->id > 0){
				$param->owner_id = $response->id; 
				$adress_book = json_decode($oApi->db_registerAdressBook($param));
				return $response;
				
			}else{
				return 0;
			}

		}

		/**
		 * [validateUserPass description]
		 * @param  [type] $data [ ->sUsername, ->sPassword ]
		 * @return [object]       [validated = bool]
		 */
		private function validateUserPass($data) {
        	$oApi = new BackendAPI($param->token);
	        $oUser = json_decode($oApi->db_getUserByNick($data)); 
	        //error_log('User in validate:'.print_r($oUser,true));
	        if(is_object($oUser->data[0])) {            
	            $_aTmp['sent'] = $data->sPassword;
	            $_aTmp['stored'] = $oUser->data[0]->password;
	            $_aTmp['user_id'] = $oUser->data[0]->id;
	            $_aTmp['validated'] = $this->oEncryptor->comparePass($_aTmp);
	            $_aTmp['user'] = $oUser->data[0];
	            return (object)$_aTmp;
	            
	        } else {
	            
	            return 0;
	            
	        }
	    }

	    /**
		 * Retrieves users data 
		 * @param  [type] $param [ ->token, ->user_id]
		 * @return obj->data[]  All data needed to display 
		 */
		public function getUsers(){
			$param = new stdClass();
			$oApi = new BackendAPI($param->token);
			$response = json_decode($oApi->db_getUsers());
			if( $response->data ){
				return $response;
			}else{
				$response = new stdClass();
				$response->error = 'Inga resultat matchade sökningen';
				return $response;
			}
		}

		/**
		 * Retrieves user data by user id 
		 * @param  [type] $param [ ->token, ->user_id]
		 * @return obj->data[]  All data needed to display char data
		 */
		public function getUserById($param){
			$oApi = new BackendAPI($param->token);
			$response = json_decode($oApi->db_getUserById($param));
			if( $response->data ){
				return $response;
			}else{
				$response = new stdClass();
				$response->error = 'Inga resultat matchade sökningen';
				return $response;
			}
		}

		/**
		 * Retrieves contact data by id 
		 * @param  [type] $param [ ->token, ->user_id]
		 * @return obj->data[]  All data needed to display char data
		 */
		public function getContactById($param){
			$oApi = new BackendAPI($param->token);
			$response = json_decode($oApi->db_getContactById($param));
			if( $response->data ){
				return $response;
			}else{
				$response = new stdClass();
				$response->error = 'Inga resultat matchade sökningen';
				return $response;
			}
		}

		    



		public function getAddressBooks($param){
			$oApi = new BackendAPI($param->token);
			$response = json_decode($oApi->db_getAdressBooks());
			if( $response->data ){
				return $response;
			}else{
				$response = new stdClass();
				$response->error = 'Inga resultat matchade sökningen';
				return $response;
			}
		}


		public function getAddressBookByOwner($param){
			$oApi = new BackendAPI($param->token);
			$response = json_decode($oApi->db_getAdressBookByOwner($param));
			if( $response->data ){
				return $response;
			}else{
				$response = new stdClass();
				$response->error = 'Inga resultat matchade sökningen';
				return $response;
			}
		}


		public function searchAddressBooks($param){
			$oApi = new BackendAPI($param->token);
			$response = json_decode($oApi->db_searchAddressBooks($param));
			if( $response->data ){
				return $response;
			}else{
				$response = new stdClass();
				$response->error = 'Inga resultat matchade sökningen';
				return $response;
			}
		}


		/**
		 * delete a contact by id 
		 * @param  [type] $param [ ->token, ->user_id]
		 * @return obj->data[]  All data needed to display char data
		 */
		public function deleteContact($param){
			$oApi = new BackendAPI($param->token);
			$response = json_decode($oApi->db_deleteContactById($param));
			return $response;
		}

		/**
		 * add contact data fro an owner 
		 * @param  [type] $param [ ->token, ->user_id]
		 * @return obj->data[]  All data needed to display char data
		 */
		public function addContactForId($param){
			$oApi = new BackendAPI($param->token);
			$response = json_decode($oApi->db_insertContact($param));

			if($response->id > 0){
				if($param->files['profile_image']['tmp_name'] != ''){
					//we have a new image AND an id from the created contact
					$param->id = $response->id;
					$param->image_url = $this->handleProfileImage($param);
				}
				$ret = json_decode($oApi->db_updateImageUrl($param));
			}

			return $response;
		}

		/**
		 * updates contact data by id 
		 * @param  [type] $param [ ->token, ->user_id]
		 * @return obj->data[]  All data needed to display char data
		 */
		public function updateContactById($param){
			if($param->files['profile_image']['tmp_name'] != ''){
				//we have a new image for existing contact id
				$param->image_url = $this->handleProfileImage($param);
			}
			$oApi = new BackendAPI($param->token);
			$response = json_decode($oApi->db_updateContactById($param));
			return $response;
		}
		

		public function handleProfileImage($data){
			//init with old url
			$resized_path = $data->image_url;

			if($data->files['profile_image']['tmp_name'] != ''){
				$tmp_file_org_name = $data->files['profile_image']['name'];
				$tmp_file_name = $data->files['profile_image']['tmp_name'];
		        $tmp_file_type = $data->files['profile_image']['type'];
		        $tmp_file_size = $data->files['profile_image']['size'];
		    
		    	$extension = pathinfo($tmp_file_org_name, PATHINFO_EXTENSION);
			    $new_image_name = $data->id.'.'.$extension;

				$file = $tmp_file_name;

	            $resized = DIR_APPLICATION.'uploads/th/th_'.$new_image_name;
	            $resized_path = PROFILE_THUMB_PATH.'th_'.$new_image_name;;
	            $original = DIR_APPLICATION.'uploads/orig/'.$new_image_name;

	            copy($file, $original);
	            $this->resizeImage($file, $resized, 250, 250);

		    }
	        return $resized_path;
		}

		/**
	     * Interface to a function which resizes images
	     * @param  [type] $file     [file handle]
	     * @param  [type] $new_file [returned file]
	     * @param  [type] $w        [width]
	     * @param  [type] $h        [height]
	     * @return [type]           [file handle]
	     */
		private function resizeImage($file, $new_file, $w, $h) {
		    require_once('tools/func.smartResize.php');
		    smart_resize_image($file , null, $w , $h , true , $new_file , false , false ,100 );
		}

	}
?>