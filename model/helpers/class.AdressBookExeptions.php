<?php

/**
 * Various extensions of the base Exception class
 */

class AdressBookException extends \Exception {
	private $userMsg;

	public function __construct(){
		$this->userMsg = '';
	}

	public function setUserMsg($msg){
		$this->userMsg = $msg;
	}
	public function getUserMsg(){
		return $this->userMsg;
	}
}

class MailerException extends \Exception {

}

?>