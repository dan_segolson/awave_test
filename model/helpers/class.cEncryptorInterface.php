<?php 

/** Core validating-logic for the application.
* The package contains functionality to compare password with user and pw-hash in db, validate it or deny and return the outcome.
* @package CoreLoginPackage
* @author dms@dmsproject.com
* @version 1.0
* @since 1.0
* @access public
*/
/**
 *  class cEncryptorInterface
 * Base class for passord and token management management in the  application
 *
 * @author dms@dmsproject.com
 * @version 1.0
 * @since 1.0
 * @access public
 */
 
class cEncryptorInterface {
    
    var $_retVal = "";

    
    /** Constructor
    * Initialises the private variables
    */
    function __construct() {
        
    }
    
    
    function hashUserPass($sPass) {
        $this->_retVal = password_hash($sPass, PASSWORD_BCRYPT);
        return $this->_retVal;
        
    }
    
    function comparePass($aCompare) {
        $this->_retVal = password_verify($aCompare['sent'] , $aCompare['stored'] );
        return $this->_retVal;
        
    }
    
    function getNewToken($length = 16){
        $sToken = openssl_random_pseudo_bytes($length);
        $this->_retVal = bin2hex($sToken);
        return $this->_retVal;
    }
     
     
     
     
}