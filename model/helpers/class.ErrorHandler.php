<?php
/**
 * The debug class holds different methods to aid in debugging, from simple error handling where you can choose to log, mail reroute from a catched error.
 */
class debug {

	public static $_method = "";
	public static $_time_start = "";
	public static $_time_stop = "";
	public static $_log = false;
	public static $_log_debug = false;

	function __construct(){

	}

	//functions to encapsule code you want to measure perfomance with
	public static function start_timer($script = 'general'){
		if(self::$_log){
			self::$_method = $script;
			self::$_time_start = microtime(true); 
		}
	}
	public static function stop_timer(){
		if(self::$_log){
			self::$_time_stop = microtime(true); 

			self::log_time();
		}
	}
	public static function log_time(){
		if(self::$_log){
			$msg = 'Elapsed in '.self::$_method.': '.round((self::$_time_stop - self::$_time_start),8).' seconds';
			self::handle_error($msg);
			self::debug_data("\r\n".$msg, 'performance.txt', 3);
			self::$_time_stop = '';
			self::$_time_start = '';
			self::$_method = '';
		}
	}

	/**
	 * handle_error - Static method to be used in place of simple error_log.
	 * 
	 * This method is intended to be used instead of traditional error_log. If you pass in an array/object or a string, it will automatically log it in a readable format. A mixed message needs to be manually handled. You have different options to handle the message, log, reroute, print a user adapted message (when implementet, also mail site owner)
	 * 
	 * @param  [type]  $data    [content to log/mail]
	 * @param  boolean $log     [true/false to log or not]
	 * @param  string $print_msg [ if not empty, it prints the msg on screen for the user]
	 * @param  boolean $reroute [true/false to reroute to fixed error page or not]
	 * @param  boolean $mail    [(Not yet implemented) true/false to mail site admin or not]
	 *
	 * @return message/log/mail/reroute
	 */
	public static function handle_error($data, $print_msg = '', $log = true, $reroute = false, $mail = false){
		if(self::$_log_debug){
			if($log){
				if((is_object($data)) || (is_array($data))){
					error_log('From framework error_handler: '.__METHOD__.' '.print_r(debug_backtrace(),true).' '.print_r($data,true));
				}else{
					error_log('From framework error_handler: '.__METHOD__.' '.print_r(debug_backtrace(),true).' '.$data);
				}
			}

			if($print_msg != ''){
				print('<div class="warning">'.$print_msg.'</div>');
			}
			if($reroute){
				//go to error page
				reroute::abort(); 
			}
			if($mail){
				//alert site admin on the error
			}
		}
		
	}

	//unpack Returns an associative array containing unpacked elements of binary string.
	//http://php.net/manual/en/function.pack.php
	//http://php.net/manual/en/function.unpack.php
	//
	/**
	 * Used to dump different types of data to file for external validation, for example binary data etc.
	 * 
	 * @param  [type]  $data     [Data to be logged]
	 * @param  string  $filename [Filename to store it under]
	 * @param  integer $type     [Flag to decide how to treat the data]
	 * @return file            [File containting the data to be examined]
	 */
	public static function debug_data($data, $filename = 'debug.txt', $type = 0){
		switch($type) {
			case '0': //string of binary data to array to csv-string
				if((is_string($data)) && (strlen($data) > 0)){
					$arr = unpack("C*", $data); //unsigned char
					self::createDebugFile($arr, $filename, $type);
				}
				break;

			case '1': //string of binary data to array and creates a csv with one value per row in file
				if((is_string($data)) && (strlen($data) > 0)){
					$arr = unpack("C*", $data); //unsigned char
					self::createDebugFile($arr, $filename, $type);
				}
				break;

			//string of binary data, unpacks and creates a java style init array
			case '2': 
				if((is_string($data)) && (strlen($data) > 0)){
					$arr = unpack("C*", $data); //unsigned char
					self::createDebugFile($arr, $filename, $type);
				}
				break;

			//takes string and writes it to file as 1:1
			case '3': 
				if((is_string($data)) && (strlen($data) > 0)){
					self::createDebugFile($data, $filename, $type);
				}
				break;
			// string and writes it to file as hex string. High nibble first
			case '4': 
				if((is_string($data)) && (strlen($data) > 0)){
					$byte_arr = unpack("H*",$data);
					self::createDebugFile($byte_arr, $filename, $type);
				}
				break;
		}
		


	
	}



	
	/**
	 * Takes incoming data from debug_data and writes it to file. filename optional, $type is passed fomr debug_data and controls the format inside the file
	 * 
	 * @param  [type] $data     [Data to be logged]
	 * @param  [type] $filename [File name]
	 * @param  string $type     [Flag to determin format inside file]
	 * @return [file]           [Stored file and log of the creation]
	 */
	public static function createDebugFile($data, $filename, $type = ''){
		$path = dirname( __FILE__).'/debug_log/';
		$content = @file_get_contents($path.$filename);	
		$debugFile = fopen($path.$filename, "wb");

		self::handle_error($content);
		switch($type) {
			case '0':
				$str = implode($data);
				break;

			case '1':
				foreach ($data as $key => $value) {
					$str .= $value.",PHP_EOL";
				}
				$str = rtrim($str,",PHP_EOL");
				break;

			case '2';
				$str = 'int arr[] = {';
				foreach ($data as $key => $value) {
					$str .= $value.",";
				}
				$str = rtrim($str,",");
				$str .= '};';
				break;

			case '3';
				$str = $content."\r\n".$data;
				break;

			case '4';
				$str = $data;
				break;

			default:
				$str = $data;
				break;
		}
		fwrite($debugFile, $str);
		fclose($debugFile);
		debug::handle_error('debug placed in '.$path.$filename);
	}

}

?>