<?php session_start();
/** Core sessionlogic for the application.
* The package contains functionality to create, read, delete sessions in DmSCmS
* @package CoreSessionPackage
* @author dms@dmsproject.com
* @version 1.0
* @since 1.0
* @access public
*/
/**
 *  class cSessionInterface
 * Base class for sessionmanagement in the DmSCmS application
 *
 * @author dms@dmsproject.com
 * @version 1.0
 * @since 1.0
 * @access public
 */
class SessionInterface{
    var $_sessionObj;
    var $_output;
    var $_errors;


    /** Constructor
    * Initialises the private variables
    */
    function __construct(){
        $this->_sessionObj = array();
        $this->_output = array();
        $this->_errors = array();
    }
    /** Creates a named session populated with an array of data
    * uses private method setSession() to actually set the session
    * @param STRING $sessionName
    * @param ARRAY $sessionData
    */
    function makeSessionArray($sessionName, $sessionData = array()){
        $this->_sessionObj[$sessionName] = $sessionData;
        $this->setSession();
    }
    /** Creates a named session populated with a string of data
    * uses private method setSession() to actually set the session
    * @param STRING $sessionName
    * @param STRING $sessionData
    */
    function makeSessionString($sessionName, $sessionData){
        $this->_sessionObj[$sessionName] = $sessionData;
        $this->setSession();
    }
    /** Transfers the named sessionObject to the actual session
    */
    function setSession(){
        $_SESSION = $this->_sessionObj;
    }
    /** Retrieves a session by name if it exists
    * @param STRING $sessionName
    * @return ARRAY with data from session (by reference) | 0
    */
    function getSession($sessionName){
        if(isset($_SESSION[$sessionName])){
            $this->_output[$sessionName] = $_SESSION[$sessionName];
        }else{
            $this->_output = 0;
        }
        return $this->_output;
    }
    /** Deletes a registered session by name if it exists
    * @param STRING $sessionName
    * @return 1|0 (by reference)
    */
    function deleteSession($sessionName){
        if(isset($_SESSION[$sessionName])){
            unset($_SESSION[$sessionName]);
            unset($this->_sessionObj);
            $this->_output = 1;
        }else{
            $this->_output = 0;
        }
        return $this->_output;
    }
}
?>
