<?php 
/**
 * Holds a multi dimensional array with nav links where the inner keys represents a nav menu option and each entry inside is a nav name/link
 * 
 * Use it by including it in the controller, the template will then access content through the keys of the array.
 * 
 * Rows commented out are either pages not yet implemented, or pages that shold not be visible in menus, but reached from other pages, like an edit button or similar.
 *
 * A new nav option here must:
 * a. Be a child of the top nav items
 * b. have a representation of the following files:
 *  - Controller in the correct controller sub folder, named as ctrl.<page>.php where <page> is for example the key of $nav_links['plugin']['key_app'] where "plugin" matches the folder in controllers, and "key_app" matches the actual controller. The controller page is then approved by the router based on the parameter ?page=<page> in the link
 *   - Template in the correct template sub folder, named as tpl.<page>.php where <page> is for example the key of $nav_links['plugin']['key_app'] where "plugin" matches the folder in templates, and "key_app" matches the actual template to use. The page is then approved by the router based on the parameter ?page=<page> in the link
 */


//top nav
$nav_links['top']['addr'] = ['name'  => 'Adressboken', 'link' => 'page/addr'];

$nav_links['top']['logout'] = ['name' => 'Logga ut', 'link' => 'home.php?action=logout'];


//Sub nav entries
//Adressbook nav options
$nav_links['addr']['list'] = ['name' => 'Sök kontakt', 'link' => 'page/list'];
$nav_links['addr']['add'] = ['name' => 'Lägg till kontakt', 'link' => 'page/add'];

//Page exists, but you navigate to them from list, not directly so they are not in the menu
//$nav_links['addr']['edit'] = ['name' => 'Editera adress', 'link' => 'page/edit'];






?>