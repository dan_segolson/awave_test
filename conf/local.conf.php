<?php
// Report only simple running errors
error_reporting(E_ERROR | E_WARNING | E_PARSE);
//debug::handle_error($_SERVER);

//Contextual path definisions
if($_SERVER['SERVER_NAME'] == 'localhost'){ 
	define('ROOT_FOLDER', '/awave_test/');	
	define('DIR_APPLICATION', $_SERVER['DOCUMENT_ROOT'].ROOT_FOLDER);
}else{
	define('DIR_APPLICATION', $_SERVER['DOCUMENT_ROOT'].'/'); 

}
//defaults to clean up logs
$js_backend_include = '';
$aSidenav = array();

//Site name etc
define('BRAND_NAME','Awave Address Book');
define('SITE_NAME','Awave Address Book');
define('BRAND_CONTACT','dan.segolson@gmail.com');

//below is if you want to switch between two databases for dev
//external database access info
define('LIVE_DATABASE_URL','localhost');
define('LIVE_DATABASE_PORT','3306');
define('LIVE_DATABASE_NAME','awave_test');
define('LIVE_DATABASE_USER','root');
define('LIVE_DATABASE_PASS','<your-password-here>');
//localhost dev db
define('DEV_DATABASE_URL','localhost');
define('DEV_DATABASE_PORT','3306');
define('DEV_DATABASE_NAME','awave_test');
define('DEV_DATABASE_USER','root');
define('DEV_DATABASE_PASS','<your-password-here>');


//internal directory paths
define('DIR_FRONTEND', DIR_APPLICATION.'frontend/');
define('DIR_TEMPLATES', DIR_FRONTEND.'templates/');
define('FORCE_HTTPS','off'); // on = turn on forced https 


if($_SERVER["HTTPS"] != "on"){
	define('PROTOCOL','http://');	
}else{
	define('PROTOCOL','https://');
}
//upload dirctories
define('PROFILE_THUMB_PATH',PROTOCOL.$_SERVER['SERVER_NAME'].ROOT_FOLDER.'uploads/th/');

?>
