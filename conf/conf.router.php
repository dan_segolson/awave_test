<?php
parse_pretty_url();
function parse_pretty_url(){
	$url =  $_SERVER['REQUEST_URI']; 
	$parsed = parse_url($url);
	$parts = explode('/',$parsed['path']);
	$key = array_search ('page',$parts);
	if($key){
		$qs = array_slice($parts,$key+1);
		if(sizeof($qs) > 1){
			foreach ($qs as $key => $value) {
				if($key == '0'){
					$_GET['page'] = $value;
				}else{
					if($key % 2 != 0){
						if($value != ''){
							$_GET[$value] = $qs[$key+1];
						}
					}
				}			
			}
		}
	}
}


/**
 * Simple switch to act as router and template selection for pages
 * uses the ?page= in GET for routing
 * The reason for the router instead of using GET directly is to prevent any page to be loaded, only the ones approved in the switch.
 */
$routingpage = 'home';
$controller = '';
$tpl_content = '';
$html_class = '';

$req_page = $_GET['page'];

switch ($req_page) {
	/**
	 * Top navigation routing
	 */
	case 'home':
		$controller = 'main/ctrl.'.$req_page.'.php';
		$tpl_content = 'home/tpl.index.'.$req_page.'.php';
		break;
	case 'addr':
		$controller = 'addr/ctrl.'.$req_page.'.php';
		$tpl_content = 'addr/tpl.index.'.$req_page.'.php';
		break;

	/*
	case 'system':
		$controller = 'main/ctrl.'.$req_page.'.php';
		$tpl_content = 'system/tpl.index.'.$req_page.'.php';
		break;*/
	/** End top navigation */


	/**
	 * Adressbook side navigation routing
	 */
	case 'list':
		$controller = 'addr/ctrl.'.$req_page.'.php';
		$tpl_content = 'addr/tpl.'.$req_page.'.php';
		break;
	
	case 'add':
		$controller = 'addr/ctrl.'.$req_page.'.php';
		$tpl_content = 'addr/tpl.'.$req_page.'.php';
		break;
	case 'edit':
		$controller = 'addr/ctrl.'.$req_page.'.php';
		$tpl_content = 'addr/tpl.'.$req_page.'.php';
		break;


	/**
	 * System side navigation routing
	 */
	/*case 'adm_config':
		$controller = 'system/ctrl.'.$req_page.'.php';
		$tpl_content = 'system/tpl.'.$req_page.'.php';
		break;*/

	
	/** System side navigation */

	default:
		if(basename($_SERVER['PHP_SELF']) != 'home.php'){
			$controller = 'main/ctrl.index.php';
			$tpl_content = 'main/tpl.login.php';
			$routingpage = 'login';	
			$html_class = 'full-height';
		}else{
			$controller = 'main/ctrl.home.php';
			$tpl_content = 'home/tpl.index.home.php';
			$routingpage = 'home';
			$html_class = '';
		}
		
		break;
}

?>