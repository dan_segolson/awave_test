<?php 
//include defines for directory paths and https on/off
require_once('local.conf.php');

//if the flag for force https is on, do it// Force HTTPS for security 
if(defined('FORCE_HTTPS') && FORCE_HTTPS == 'on'){
    if($_SERVER["HTTPS"] != "on") {
        $pageURL = "Location: https://";
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        header($pageURL);
    }
}
//include session handler, base controller and main admin model
require_once(DIR_APPLICATION.'model/class.SessionInterface.php');
require_once(DIR_APPLICATION.'controller/class.BaseController.php');
require_once(DIR_APPLICATION.'model/class.adminModel.php');
require_once(DIR_APPLICATION.'model/helpers/class.AdressBookExeptions.php');
require_once(DIR_APPLICATION.'model/helpers/class.ErrorHandler.php');


?>