<?php 
//include navlinks
require_once(DIR_APPLICATION.'conf/conf.nav_links.php');
require_once(DIR_APPLICATION.'controller/main/class.MainController.php');
//model is included in main_conf

/**
 * Sets up basic vars which the template uses
 * Can handle standard serverside login, commented out due to handling 
 * basic auth through firebase javascript SDK so that part gors through process.php
 */

$pagerole = 'login'; //decides visibility of page specific elements
$pagename = SITE_NAME.' - Login'; // Top of page name
$navstyle = 'navbar-light'; //adds right styling to top nav 
$html_class = 'full-height';
$navkey = 'login'; //key to select right nav options from the nav-links array


?>