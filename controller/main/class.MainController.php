<?php 

/**
 * Main controller
 * Handles selection of various actions specific to the index page.
 * Extends BaseController which holds common controller functions such as login/logout/auth etc
 */
class MainController extends BaseController {

	public function __construct() {
		parent::__construct();
	}


	/**
	 * Looks at the action and data passed fom the template controller
	 * and acts accordingly
	 * @param  STRING $action login/logout
	 * @param  OBJECT $data   Object holding data from POST
	 * @return MIX         Either the created object, true/false or a string representing the error
	 */
	public function selAction($action,$data) {
		
		switch($action){
			case 'logout':
				$error = '';
				if($this->logout($data)){
					return '{"status":"ok"}';
				}else{
					$error = '{"status":"Logout failed"}';
					return $error;
				}
				break;
			case 'login':
				$error = '';
				if($this->login($data)){
					return 1;
				}else{
					$error = 'Login failed, please check username and password.';
					return $error;
				}
				break;

			case 'register':
				$error = '';
				$reg = $this->register($data);	
				if($reg->id > 0){
					return json_encode($reg);
				}else{
					$error = 'Register failed, please check your entered information.';
					return $error;
				}
				break;
			
			default;
			break;
		}

	}

	
}

?>