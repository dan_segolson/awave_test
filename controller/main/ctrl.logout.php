<?php 
//include navlinks
require_once(DIR_APPLICATION.'conf/conf.nav_links.php');
//model is included in main_conf
$oModel = new AdminModel(); //use this to retrieve data from api

$pagerole = 'sport'; //decides visibility of page specific elements
$navkey = 'sports'; //key to select right nav options from the nav-links array
$pagename = 'Administration - Logout'; // Top of page name
$navstyle = 'navbar-dark double-nav'; //adds the right styling to top nav 
$aSidenav = $nav_links[$navkey]; //navlinks array comes from included conf


?>