<?php 
//include navlinks
require_once(DIR_APPLICATION.'conf/conf.nav_links.php');
require_once(DIR_APPLICATION.'controller/home/class.HomeController.php');
//error_log(print_r($_SESSION,true));

$pagerole = 'home'; //decides visibility of page specific elements
$pagename = 'Overview'; // Top of page name
$navstyle = 'double-nav'; //adds right styling to top nav 

//$navkey = 'home'; //key to select right nav options from the nav-links array
//$aSidenav = $nav_links[$navkey]; //navlinks array comes from included conf


$oContr = new HomeController();

if($oContr->authUser()) {
	$username = $oContr->getUserName(); 
	$role = $oContr->getUserRole();


	//catch post & action for controller work
	if($_GET['action'] != '') {
		$action = $_GET['action'];
		$data = (object)$_POST;
		$users = $oContr->selAction($action,$data);	
	}
	
}else{
	$oContr->redir('index.php');
}



?>