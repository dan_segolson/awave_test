<?php 

/**
 * System controller
 * Handles selection of various actions specific to the system pages.
 * Extends BaseController which holods common controller functions such as login/logout/auth etc
 */
class SystemController extends BaseController {

	public function __construct() {
		parent::__construct();
	}


	public function selAction($action,$data) {
		
		switch($action){
			case 'logout':
				$this->logout();
				break;
			case 'setEnv':
				$this->setEnvironment($data);
				break;
			default;
			break;
		}

	}


	public function setEnvironment($data){
		if($data->sel_environment == 'stage'){
			$this->oSession->makeSessionString('USE_ENV', 'STAGE');
			//redir to use the session just set and trigger a login
			$this->redir('home.php?page=adm_config');
		}else{
			$this->oSession->makeSessionString('USE_ENV', 'LIVE');
			//redir to use the session just set and trigger a login
			$this->redir('home.php?page=adm_config');
		}
	}
}

?>