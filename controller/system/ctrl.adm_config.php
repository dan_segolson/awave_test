<?php 
//include navlinks
require_once(DIR_APPLICATION.'conf/conf.nav_links.php');
require_once(DIR_APPLICATION.'controller/system/class.SystemController.php');

$pagerole = 'system'; //decides visibility of page specific elements
$pagename = 'Oddsium - System Admin configs'; // Top of page name
$navstyle = 'navbar-dark double-nav'; //adds the right styling to top nav 

$navkey = 'system'; //key to select right nav options from the nav-links array
$aSidenav = $nav_links[$navkey]; //navlinks array comes from included conf


$oContr = new SystemController();

if($oContr->authUser()) {

	$username = $oContr->getUserName();

	//catch post & action for controller work
	if($_GET['action'] != '') {
		$action = $_GET['action'];
		$data = (object)$_POST;
		$response = $oContr->selAction($action,$data);	
	}
	
}else{
	$oContr->redir('index.php');
}

?>