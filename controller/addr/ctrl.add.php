<?php 
//include navlinks
require_once(DIR_APPLICATION.'conf/conf.nav_links.php');
require_once(DIR_APPLICATION.'controller/addr/class.AddrController.php');
//model is included in main_conf
$oModel = new AdminModel(); //use this to retrieve data from api

$pagerole = 'addr'; //decides visibility of page specific elements
$pagename = 'Adressbook - Add'; // Top of page name
$navstyle = 'navbar-dark double-nav'; //adds right styling to top nav 

$navkey = 'addr'; //key to select right nav options from the nav-links array
$aSidenav = $nav_links[$navkey]; //navlinks array comes from included conf


$oContr = new AddrController();

if($oContr->authUser()) {

	$username = $oContr->getUserName();
	$role = $oContr->getUserRole();
	$user_id = $oContr->getUserId();
	if($role == 1){
		$users = $oContr->getUsers();	
	}
	
	//catch post, files & action for controller work
	if($_GET['action'] != '') {
		$action = $_GET['action'];
		$data = (object)$_POST;
		$data->url_id = $_GET['id']; 
		$data->files = $_FILES; 
		$response = $oContr->selAction($action,$data);	
	}
	
}else{
	$oContr->redir('index.php');
}
?>