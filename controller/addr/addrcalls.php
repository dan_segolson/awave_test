<?php
if($_SERVER['SERVER_NAME'] == 'localhost'){ 
	require_once('../../conf/main_conf.php');
}else{
	require_once('../../conf/main_conf.php');
	//require_once($_SERVER['DOCUMENT_ROOT'].'conf/main_conf.php');
}
require_once(DIR_APPLICATION.'controller/addr/class.AddrController.php');

/**
 * API of sorts for ajax calls from the pages, grabs action from url and payload from post and passes it to sports controller for action.
 * Prints response for the ajax call to read and act on.
 */

$oCont = new AddrController();
//catch post & action for controller work
if($_GET['action'] != '') {


	$action = $_GET['action'];
	$data = (object)$_POST;
	$response = $oCont->selAction($action,$data);
	print($response);
}

?>