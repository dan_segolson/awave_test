<?php 

/**
 * AddrController
 * Handles selection of various actions specific to the adresses
 * Extends BaseController which holods common controller functions such as login/logout/auth etc
 */
class AddrController extends BaseController {

	public function __construct() {
		parent::__construct();
	}


	public function selAction($action,$data) {
		
		switch($action){
			case 'logout':
				if($this->logout($data)){
					$this->redir('index.php');
				}else{
					$error = '{"status":"Logout failed"}';
					return $error;
				}
				break;

			case 'find':
				return $this->getAddressList($data);
				break;

			case 'load':
				return $this->getContactById($data);
				break;

			case 'update':
				return $this->updateContactById($data);
				break;
			

			case 'add':
				return $this->addContactForId($data);
				break;

			case 'delete':
				return $this->deleteContact($data);
				break;
			
			default;
			break;
		}

	}

	

	public function getAddressList($data){
		if($this->getUserRole() == 1){
			//we be admin
			if($data->search == ''){
				return $this->oModel->getAddressBooks($data);
			}else{
				//we have search param, so do it
				return $this->oModel->searchAddressBooks($data);
			}
		}else{
			$data->owner_id = $this->getUserId();
			if($data->search == ''){
				return $this->oModel->getAddressBookByOwner($data);
			}else{
				//we have search param, so do it
				return $this->oModel->searchAddressBooks($data);
			}
		}	
	}
	

	public function getContactById($data){
		if($data->url_id != ''){
			$data->id = $data->url_id; //to avoid override of id in post object on update
		}
		return $this->oModel->getContactById($data);
	}

	public function updateContactById($data){
		$ret = $this->oModel->updateContactById($data);
		if($ret->status == 'Uppdaterad'){
			$retVal = $this->getContactById($data);
			$retVal->status = $ret->status;
			return $retVal;
		}else{
			return $ret;
		}
	}

	public function addContactForId($data){
		$ret = $this->oModel->addContactForId($data);
		if($ret->id > 0){
			$data->id = $ret->id;  
			$retVal = $this->getContactById($data);
			$retVal->status = 'Kontakt sparad med id: '.$ret->id;
			return $retVal;
		}else{
			return $ret;
		}
	}


	/* NOTE This is called from AJAX */
	public function deleteContact($data){
		$response = '';
		$ret = $this->oModel->deleteContact($data);
		if($ret->rows > 0){
			$response = '{"status":"Kontakten permantent borttagen.","rows":"'.$rows.'"}';
		}else{
			$response = json_encode($ret);
		}
		return $response;

	}


	public function getUsers(){
		return $this->oModel->getUsers();
	}
	
	
}

?>