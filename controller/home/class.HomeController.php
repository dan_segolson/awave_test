<?php 

/**
 * Home controller
 * Handles selection of various actions specific to the home page.
 * Extends BaseController which holods common controller functions such as login/logout/auth etc
 */
class HomeController extends BaseController {

	public function __construct() {
		parent::__construct();
	}


	public function selAction($action,$data) {
				
		switch($action){
			case 'logout':
				if($this->logout($data)){
					$this->redir('index.php');
				}else{
					$error = '{"status":"Logout failed"}';
					return $error;
				}
				break;
			

			default;
			break;
		}


	}


}

?>